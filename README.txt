Domain name registered with regery, login is the contact@vicdebsoc google account

Dependancies
	Hugo https://gohugo.io
	StaticJSCMS https://www.staticcms.org/ (editing interface)
	Gitlab (hosting & login (github may sort their shit out eventually))
	Pagefind https://pagefind.app/ (search)
	Jampack https://jampack.divriots.com/ (optimisation)
	Mailchimp for the newsletter signup



I developed this primarily using firefox 118-122, with some consideration for the contemporaneous browsers. Because of this I was not able to make use of some of the more advanced CSS. That being said, I did use some of the newish features, like pseudo selectors and fine grained styling attributes (like text-decoration-offset). This website is designed assuming that most people using it are probably uni students with fairly up to date technology.
That may prove to be a bad assumption.

Feel free to contact me for more help.
I've included my email and phone in the private reference docs https://docs.google.com/document/d/1Oi882dU60LaYU1thMiz8bR0JrzLJ3yyqoWLNSFyJ2dw/edit?usp=sharing