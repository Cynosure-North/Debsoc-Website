---
layout: about
title: About
url: /about
---

The Society was formed in 1899, in anticipation of the opening of the new Victoria College, part of the then University of New Zealand. Students who formed the Debating Society also went on to form the Victoria Students’ Association.

The Society has a long record of participation and success on the international stage. Teams from the Society have made the Grand Final of the Australasian Intervarsity Debating Championships in two of the last three years. At the end of 2008, a team from the Society reached the semi-finals of the World Universities’ Debating Championships. In 2007, a team from Victoria won the Cambridge Union IV and reached the final of the Oxford IV. The Society is currently ranked as the 19th best university in the world at debating.

The Society is a non-profit incorporated society, a registered New Zealand charity, and an affiliated club within the Victoria University of Wellington Students’ Association. It is led by an Executive Committee of eleven members, and governed by a constitution, which you can find linked below The Society is heavily involved with debating and public speaking events around Wellington, participating in a number of public debates for various organisations and taking a leading role in organising debating at a secondary school level.

We meet every {{< details/day >}} in {{< details/room >}} at {{< details/time >}}. Come along, no experience necessary.

## Our Events

### Vic Champs
Vic Champs is held at at a cheap Wellington BYO restaurant and always seems to be attended by a good number of wine bottles. This event farewells the first trimester, and gets the society together for one last time before the holidays.
6 first year debaters tackle a ridiculous motion for the entertainment of the rest of the members.

### AGM
The Annual General Meeting of the Society is an important event at the end of the year. The President and Treasurer report on the year, and the committee for the following year is elected. Whether you are running for committee or not, make sure you come along – there’ll be pizza, and possibly political intrigue!

### Regressive
The final event of the year, it's traditionally held in the Kelburn Village Scout Hall. 6 entertaining debaters are selected to ~~embarrass themselves~~ entertain the crowd. It's perfect way to send off the year and remember all the good times.

A regressive debate starts with the adjudication and progresses backwards from there, all the way to the chairperson welcoming everyone to the debate. As you’ll expect this event is absolutely hilarious. Audience participation guaranteed; and the debaters have usually had some liquid refreshments in advance to make things funnier/more debauched.