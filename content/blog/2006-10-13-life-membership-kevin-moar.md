---
layout: blog
title: Life Membership - Kevin Moar
publishDate: 2006-10-13
---
## KEVIN MOAR

Nomination for Life Membership of the Victoria University of Wellington Debating Society

Citation read by Christopher Bishop and Gareth Richards at the Annual General Meeting of the Society

Thursday 13 October 2006

Kevin Moar is a true legend of Debsoc and embodies all that is good about the Victoria University Debating Society. Kev has been a member of the Society since 1998 and has been one of Vic’s most successful debaters and adjudicators. He has also put an enormous amount back into the Society and helped make it what it is today – the most successful and feared Debating Society in New Zealand. We therefore have great pleasure in nominating him for life membership of the Society.

Kev first joined the Society when arriving at University in 1998, a fresh-faced ex-Scots College debater of no particular repute in the Premier A grade in Wellington. His great friend, Duncan Small, says: “Kev and I started debating at Vic in 1998 together. Neither of us had particularly auspicious starts, I don’t think that Kev was selected for his first Easters. He was great fun tho. Under the guidance of Graham Cameron (in his wild days), Rob, Matt, Chris, Hamish, Ange et al, Kev took to debating ’society’ like a duck to water. He lived over in Weir House, we got pissed as chooks. He had an exceptionally bad wardrobe and yellow tinted Lennon glasses.”

### Debating

Kev attended the Australasian Championships held in Wellington in 1998 and developed quickly as a debater thanks to the renowned Debsoc training programme. From 2000 to 2002 he formed part of the legendary “awesome foursome” (with Duncan Small, Chelsea Payne and Nikki Willis) that dominated New Zealand University debating, made Vic the most feared Society in the country and inspired countless school students to do debating at university – including this writer.

Kev was a first speaker. However he himself will admit that he “didn’t really get how to speak first” until after he went into semi-retirement as a debater. Despite this, Kev’s first speeches (his first negatives in particular) were a vital part of a great period of Vic domination of university debating. His summaries in particular were a sight to behold. In full flight Kev’s oratory could spellbind a crowd and debates were frequently turned by Kevin’s impassioned summations.

After a couple of near misses, he won Easters in 2001 and as a member of Vic 2 at Joynt Scroll, beat Victoria 1 to knock them out of the break and indirectly cause the famous Massey-Massey final. In 2002, Kev took the big step up from the giantkiller in Vic 2 to the mature team leader in Vic 1. He won Joynt Scroll in 2002 in Wellington. Duncan Small notes with a tinge of regret that “this was the last time we debated together and Kev was totally on top of his game, as a first speaker with guile, knowledge and kick-ass summaries.”

Kev was also named as a member of the New Zealand Universities Prepared Debating Team in 2001. In 2003 Kevin won the Parliamentary Shield, the prize for premier grade adult debating in Wellington.

### Adjudication

It is probably in the field of adjudication that Kev has made his greatest mark in debating. Kevin won the Centennial Cup for Best Adjudicator in a NZU competition at Easters in 2003. He was the Chief Adjudicator of Joynt Scroll 2003 and the Chief Adjudicator of the inaugural Victoria University BP Tournament in 2004 and then also in 2005.

Kev has been a stalwart for Victoria, and New Zealand, at the World Universities Debating Championships. He has judged the break at five different tournaments, including up to and including the semi-finals in South Africa in 2002, as well as the ESL Grand Final in 2003 in Singapore.

At the Australasian Intervarsity Debating Championships his crowning achievement was being Chief Adjudicator of the largest Australs ever in Wellington earlier this year. He also judged the Grand Final in 2005 in Queensland.

### Service

Kevin has given back to Vic Debsoc more than can be written in a short blurb. He has taken time to sit on the Debsoc committee at various times and provided valuable service there. However, it is the contribution that he has made outside of these committee roles that truly distinguish him and make life membership of the Society a worthy honour.

Kevin’s debating and adjudicating experience outlined already are tremendous, but it is his generosity in allowing year after year of debaters to tap into that knowledge that has been truly remarkable. Many Debsoc members use their move into paid employment as an excuse to shun the watery Eastside beer and overheated SUB rooms and thus stay away from Debsoc. Kevin, however, when asked to help out on a club night will be present there at the drop of a hat. He has always been willing to adjudicate, select, deconstruct demonstration debates and run seminars on debating in general or different debating styles. His presence at debating has in some years been more consistent than many committee members. You really cannot ask for more of an individual in a debating society.

Once in the Debsoc environment his enthusiasm towards speakers is unbounded. He is always willing to offer advice on debating to individuals or groups if asked and we can say personally that as young speakers we found this advice tremendously helpful and we’re sure we could find tens of other debaters who would echo these comments.

Within this sphere he has been particularly crucial in developing Victoria’s involvement and successful involvement in international tournaments. Kevin’s continued presence at Australs and Worlds has meant that he has built up a great database of knowledge on these styles. He has been invaluable to the Society in passing on this knowledge to debaters and allowing Vic debaters to not see the jump from domestic to international competitions as being one which they shouldn’t make. Victoria’s participation and success at Worlds and Australs over the last two to three years, has to a great degree relied on Kevin’s knowledge of these styles and the advice that he has been able to give to competitors about the Australs or BP style. Once in the Australs or Worlds environment his reputation as an excellent judge has rubbed off on Vic teams and allowed us to be viewed as credible competitors.

Those who have attended a Worlds or Australs offshore will know that having a ‘standing’ as an institution is extremely important because it allows you to gain good judges and be viewed as potential breaking teams. Kev’s contribution has truly been invaluable in this regard. Kev was integral to Vic being able to host Australs this year. It was Kevin who was one of the key protagonists in forming a bid committee. When we came to bid we were up against a high quality bid from UT Mara which was going to be a less expensive tournament for many universities to attend. Having Kevin as our CA and having a high quality DCA and Tab team which Kevin had recruited ensured that we were able to take votes from Asian universities and meant that we won the bid handsomely.

Kevin’s enthusiasm towards the club in other ways has also been unparalleled. He frequently acts as a taxi for debaters whether it be to a committee meeting or to some hick town like Palmerston North or Hamilton to get younger debaters to tournaments. Vic Debsoc wouldn’t be the amazing club that it is if it was solely a place for debating every Wednesday. Kevin’s love of a good time and his stewardship over the Vic Debsoc Buffalo Club has meant that Vic Debaters don’t just try and beat each other in debates, but also with trickery in a drinking game. Very few people will devote as many years and as many hours within those hours to a pursuit like debating. Even fewer will do so when the opportunity for them to debate is minimal and they solely fulfil an adjudicating and mentoring role. Despite this Kevin Moar has continued to be an amazing asset to this debating society – and so we heartily commend that he be made a life member of this society.
