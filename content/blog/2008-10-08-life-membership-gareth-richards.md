---
layout: blog
title: LIfe Membership - Gareth Richards
publishDate: 2008-10-08
---
## GARETH RICHARDS

Nomination for Life Membership of the Victoria University of Wellington Debating Society

Citation read by Christopher Bishop at the Annual General Meeting of the Society

Wednesday 8 October 2008

Gareth is nominated for life membership of the Victoria University of Wellington Debating Society for outstanding debating and adjudication achievements, and superb service to the Society over a period of many years.

### Debating

Gareth joined the Society in 2002 as a fresh-faced Weir-House attending, ex-Palmy North Boys, ex-schools Nationals boy. Having won a handsome one out of five debates at Nationals the previous year and with an already unnatural affinity for the bottle I don’t imagine, looking back, Gareth thought he would scale the debating peaks he did. He was elected as the 1st year rep – except the President forgot to tell him when and where the committee met. His debating rescue came through Sayeqa Islam who hassled him enough until he attended the Pro-Am tournament, which he won. Joynt Scroll 2002 was also an unhappy experience, with an ill-prepared and dysfunctional team. A very young Gareth even missed the social functions – nobody told him they were on!

Thankfully 2003 brought better organisation and happier times. Gareth’s first Easters was in Massey and was where I first met Gareth properly. We bonded over James Butchard’s whisky on the first night. Unfortunately that is all I recall about that tournament, and I don’t think Gareth remembers much more. Gareth’s first Worlds came at the end of 2003 when Vic sent a large squad to Singapore. It was my first time debating with Gareth. We were thrown together as partners as essentially the odd ones out in the squad. Despite a disastrous opening two days we went 9/9 on the final day and actually finished in the top 50 – even though our speaker rankings had us in the mid 250s!

Singapore Worlds is especially remembered amongst Vic for the comically puritanical attempts to stop almost all alcohol consumption. Buying a bottle of the wine at the Championship Dinner involved queueing in three separate queues, using three different tickets, and paying by cash only. This did not deter Gareth – it was at this tournament where Gareth delivered the famous red-wine vomit outside the hotel and where to the surprise of many present he declared later that “Phil Cornege can be a good guy”.

Gareth’s first tournament victory came at Easters in 2004, when he and I beat Otago in front of a young Polly Higbee, just entering her 7th form year at St Hilda’s Collegiate (Later that year Polly, having been inspired by the spoken word, would deliver such a devastating “speech from the floor” at Nationals that we had to ban them). After a brief and drunken sojourn to Sydney for Australs in 2004, Gareth’s next debating adventure took him back to his spiritual homeland – to the mighty Palmy. Making full use of the Richards’ family facilities and avoiding at all costs the pubs of his childhood, we made the final, only to lose (in my opinion outrageously, in Gareth’s phlegmatic view, justifiably) to Otago Two with Marcelo Barry Rodriguez-Ferrere in full-on white-guilt mode about Treaty Settlements. Gareth was named as 1st reserve to the NZ team.

The first NZ BP tournament was held at the end of 2004. We won the final because Kevin set a topic so outrageously neg-weighted that as long as we remembered to speak English, we would win. Thankfully our speaking powers didn’t desert us and we walked away with the prize for the inaugural tournament which would become the NZ British Parliamentary Debating Championships.

Gareth returned to Worlds at the end of the year, again with me as a partner, to Malaysia Worlds. I have many happy memories of this tournament – beating Jono and Jesse from Auckland in rounds 4 and 7 (although losing to them in round 9 and the octo-final), breaking 25th after thinking we’d screwed up the final round, and visiting the island resort of Langkawi after the tournament. Two particularly unhappy memories are Gareth giving me a large full plastic cup on break night and telling me to scull it as it was vodka and lemonade, when actually it was straight gin, and missing the plane to Langkawi, which although not all Gareth’s fault, in accordance with the doctrine of collective responsibility he knows so much about, he must be partially blamed.

Having won Easters already and not being a glutton for punishment Gareth gave the 2005 tournament on the North Shore a miss, but he returned for Australs 2005 when he, Joe and I made the quarters, beating Sydney 1 in the octo-finals. The best speech I have seen Gareth give came at this tournament against IIU 1 in round seven – a must win for us. Asked to affirm “That we should pay for the return of hostages from Iraq” Gareth refused to be cowed by the “we are Muslim/you are Western and ignorant” arguments from the opposition and totally smashed them from 3rd affirmative, getting a 79 from a DCA and ensuring we broke.

Joynt Scroll 2005 was disappointing for Gareth in a team-sense as Victoria One went out (along with Victoria Two) in the semi-finals, but he was named as captain of the NZU Prepared team, after a superb tournament. That sadly ended Gareth’s Joynt Scroll debating career and I know it is much to his chagrin that he retires having never won the famed Scroll. The BP tournament at the end of the year partially made up for it, as Gareth and I defended our title against teams from around NZ. We had another go at Worlds at the end of 2005, frustratingly finishing in 35th place and missing the break on speaker points.

The bulk of Gareth’s 2006 and 2007 were noticeably absent from competitive debating, although he did win Parliamentary Shield in 2007. Gareth made a successful comeback to debating at the tail end of 2007. He and I won the NZ BP Champs for the third time, and he won the best speaker prize for the first time. We went to our final Worlds quite confident of doing well and we broke 20th, after yet another near-death experience in the final round about medical ethics. Sadly we went out in the octo-finals, but Gareth finishes his Worlds career as one of only a few New Zealanders to break twice at Worlds. It has been an absolute pleasure debating and travelling with him to and at overseas tournaments.

### Adjudication

Gareth’s adjudicating debut came when he went to Easters on the North Shore in 2005. He took on the role of Camp Dad and was efficient in distributing berocca in the morning to the various log cabins/hovels we were sleeping in. This was a particularly debauched tournament when Gareth was at the height of his “Rareth” infamy. On the first day walking to Auckland “University” of Technology, Gareth spotted a McDonalds and made a big deal about saying we needed to go back there later because MacDos was delicious. Many people were confused later when on the way home Gareth again became excited about seeing the very same McDonalds. When it was pointed out that he had seen the same restaurant only a few hours earlier and said similar things Gareth ruefully admitted he could not recall such excitement. Indeed he found it hard to recall rounds one and two of the tournament (but he was judging Otago and Canterbury, so it was of no matter). Thankfully he was not a selector. Despite such alcohol-induced handicaps Gareth made it all the way as a judge to the the final of that Easters.

After that (in)auspicious debut Gareth quickly gained a reputation as one of the best, and fairest judges on the New Zealand debating circuit. He won the Centennial Cup for Best Adjudicator at Easters 2006, where he also acted as a DCA, and he won the cup again at Joynt Scroll 2008. At Australs in Wellington in 2006 he was given the honour of adjudicating the Grand Final, and he remains one of only a few New Zealanders to have achieved such an honour. He was the CA of the NZ BP Champs at the end of 2006, and did so again in 2008. He has adjudicated the final of Joynt Scroll, been an NZUDC selector, assisted with trainees, written guides on how to accredit – in short, Gareth has done it all as an adjudicator, and always with a smile.

### Service

After the afore-mentioned somewhat forgettable year as 1st year rep on the Society’s committee, Gareth rose through the Debsoc ranks: general exec in 2003, Vice-President in 2004, all the way to President in 2005. Gareth’s time as President is perhaps most notable for the decision to bid for the Australasian Intervarsity Debating Champs in July 2005 (which was held the next year, in 2006). It would be nice to say that this decision was taken rationally and calmly after full feasibility studies and investigation by the Society, but it was actually taken after Gareth and I stole a philosophy student’s bottle of Gin at one of Ranald’s famous Aurora Terrace parties, and were too embarrassed to back out after we’d told everyone we were bidding. Thankfully it all worked out well in the end.

Gareth played an absolutely vital role in the organisation of Australs in Wellington. He served as deputy convenor and the co-ordinator of the myriad social events we arranged: from opening night drinks, break night, the final night party in the Boatshed, American Independence party, Championship Dinner, etc. He kept me sane and caffeinated, he read (and deleted) my crazy text messages of ideas sent at 3am in the morning, he edited and wrote most of the tournament handbook, and he made sure everyone had a kick-ass time. In short, he was the major contributor to why Australs 2006 is remembered as one of the best ever.

After finishing up with Debsoc administration Gareth went onto be Secretary-Treasurer of NZUDC for eighteen months, and was a NZUDC selector on a number of occasions. Then there are all the little, but bloody important, things: the Pro-Am “pro-ing” and judging, the selection of Vic teams for Easters, Joynt, and other tournaments, the sending around of emails to squad members with topic ideas for tournaments and his thoughts on current issues, and so much more.

### Conclusion

At time of writing in 2008, the Society holds ever domestic debating title in New Zealand, and the last three tournaments have all been Victoria v Victoria finals. We are breaking teams at Worlds and Australs regularly. The Society has more members than ever. We are active in the community. We are intensely competitive, but collegial and friendly.

Gareth Richards has been a massive part of the rise and rise of the Society over the last six years. He is, and was, a fantastic debater and adjudicator, and helped make the Society what it is today. He is richly deserving of the honour of life member.
