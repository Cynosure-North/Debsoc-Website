---
layout: blog
title: Life Membership - Sayeqa Islam
publishDate: 2008-10-08
---
## SAYEQA ISLAM

Nomination for Life Membership of the Victoria University of Wellington Debating Society

Citation read by Christopher Bishop on Wednesday 6 August 2008

Nomination accepted by the Annual General Meeting of the Society on 8 October 2008

Sayeqa is nominated for life membership because of her extraordinary debating achievements over a long period of time, her contribution to the Society and NZ debating through adjudication, and her work as a debating administrator and organiser.

### Debating

Sayeqa has had a long and distinguished debating career for the Victoria University of Wellington Debating Society. Sayeqa joined the Debating Society in 2000, having achieved success, as many Debsoccers do, at a school level for Wellington Girls’ College and the NZ Schools’ Debating Team. It took Sayeqa a while to really find her debating feet at University. The Society was going through a very successful period. Domestic debating was dominated by titans like Kevin Moar, Nicola Willis, Chelsea Payne, and Duncan Small and there were not many chances to shine.

Sayeqa’s first tournament victory came in 2003 when her and Ranald Clouston, as Victoria One, defeated the much-fancied Auckland One team at Easters in Palmerston North. This Grand Final is still much talked about more than six years later and has entered Debsoc folklore. Auckland had gone through the rounds unbeaten, and had inflicted a heavy defeat on Vic during the preliminary rounds. The Auckland speakers were later named as the two best speakers of the tournament, but Victoria inflicted a 7-0 thumping of them in the final, after Auckland imploded and set up one of the most bizzare models ever seen in an Easters final.

Later that year Sayeqa combined with Ranald and myself to retain the Joynt Scroll for Vic in Christchurch. We went through the rounds unbeaten. Sayeqa was in blistering form at this tournament, speaking third - a position that was hers in any team she spoke in. I was only in first year. It was a pleasure to debate with her, and I learned a lot. She was named as a member of the NZ Universities’ team, an honour that was richly deserved, and she received a NZU Blue for her efforts in 2003.

To summarise, Sayeqa’s debating CV consists of:
 - Winner of Easters 2003
 - Winner of Joynt Scroll 2003; NZU Prepared Team
 - Winner, Parliamentary Shield 2001; Runner-Up, 2003
 - Winner, Vic Shield 2006 and 2007;
 - Best Speaker, NZ BP Champs 2005 and 2006
 - Quarter-finalist, Australs 2006 and 4th best speaker
 - Runner-Up, Australs 2007; Best Speaker; Best Speaker in the Grand Final
 - Runner-Up, Women’s 2007; Best Speaker
 - Winner, Cambridge IV 2007; 2nd best speaker;
 - Runner-Up, Oxford IV 2007
 - Runner-Up, UCD IV 2007; 6th best speaker;
 - Octo-finalist, Worlds 2005
 - Three NZU Blues for debating (2003, 2006, 2007);
 - Three Victoria blues for debating (2003, 2006, 2007), including Sportsperson of the Year (2007)


*It seems the later portions of Sayeqa’s citation have been lost to history. We regret this.*
