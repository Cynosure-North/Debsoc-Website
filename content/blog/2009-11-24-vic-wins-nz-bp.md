---
layout: blog
title: Vic Wins NZ BP
publishDate: 2009-11-24
---
Vic has come out victorious at the NZ Open British Parliamentary Champs from the 20 - 22 November.

The team 'Two men and one moral high horse' of Udayan Mukherjee and Paul Smith won the Grand Final in Parliament's Legislative Council Chambers from closing opposition on the motion This House would ban criminals from publishing accounts of their crimes.

Many thanks go to Gareth Richards, the Chief Adjudicator, and to Sarah Wislon, Nigel Smith, and Alex Sinclair for their work as co-conveners.


The full results are:
Winners: Udayan Mukherjee, Paul Smith

Grand Finalists : Polly Higbee, Stephen Whittington

Semi Finalists:

Kathy Scott-Dowell, Richard D'Ath;

Josh Cameron, Holly Jenkins;

Jono Orpin, Ellen Thomson (Composite)

Most Promising Speaker: Ellen Thomson

Top 10 Speakers:

Stephen Whittington (2nd)

Polly Higbee (3rd)

Clodagh O'Connor-McKenna (4th)

Josh Cameron (5th =)

Richard D'Ath (5th =)

Hugh McCaffrey (9th =)
