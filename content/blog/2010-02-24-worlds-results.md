---
layout: blog
title: Worlds Results
publishDate: 2010-02-24
---
Vic has had another good outcome at Worlds in Antalya, Turkey over the New Year.

On the team front, Vic A (Polly Higbee, Stephen Whittington) broke to the octo-finals where they lost out to Kingston A and eventual Grand Finalists, the London School of Economics A. Both Vic B (Seb Templeton, Ella Edginton) and Vic C (Kathy Scott-Dowell) needed only one more point to make the break.

On the judging front, all four Victoria judges (Chris Bishop, Jono Orpin, Gareth Richards, Jesse Wilson) broke - with Jono judging a semi-final, and Chris all the way to the Grand Final.

On the speaker tab Vic had four speakers in the top fifty in the world out of 800 competitors. Stephen Whittington and Ella Edginton were 41st equal, Polly Higbee was 37th, and Seb Templeton was 18th.
