---
layout: blog
title: Easters Squad Announced
publishDate: 2010-03-28
---
Congratulations to the squad selected for Easters this April. Thanks to all who trialed, Ihaia for swinging, and Ranald, Bish, Clodagh, Polly, Gareth for selecting.

Vic 1: Udayan Mukherjee, Paul Smith

Vic 2: Seb Templeton, Richard D'Ath

Vic 3: Sam Ward, Tom Matthews

Vic 4: Nick Cross, Asher Emanuel

Vic 5: Daniel Wilson, Lauren Brazier

First reserve: Frances Ratner
