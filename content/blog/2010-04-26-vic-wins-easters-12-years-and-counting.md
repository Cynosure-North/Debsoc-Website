---
layout: blog
title: Vic wins Easters - 12 years and counting
publishDate: 2010-04-26
---
Great news from Invercargill. Victoria has won the Officers Cup for the twelfth year in a row. The mighty Victoria Two team (Seb Templeton and Richard D'Ath) took down Auckland Two (Steph Thompson and Akif Malik) in the Grand Final in a 4-1 decision. Victoria One (Udayan Mukherjee and Paul Smith) also broke (in 1st place) but lost to Auckland Two in a close 2-1 semi-final decision.

Victoria dominated the individual speaker awards. Richard was named as the Easters best speaker for the second year in row (winning the Dame Cath Tizard Cup) and also made the NZ Universities' Impromptu Team for the third year in a row. Joining him in the team is Udayan Mukherjee, who was also named as captain. Seb Templeton was named as the first reserve.

Paul Smith won the Russell McVeagh Cup for the Most Promising Speaker, and Sam Ward won a promising award. Yogesh Patel won the Centennial Cup for best adjudicator, and Daniel Wilson won the Sir David Beattie Cup for public speaking. Victoria won every trophy going!

Seb Templeton joins an elite club of people to win the Officers Cup twice after his victory last year (other Victoria members of the club - Stephen Whittington who won in 06 and 07, Christopher Bishop in 04 and 05, Chelsea Payne in 00 and 02, Nicola Willis in 01 and 02, Rob Salmond in 96 and 97, and Ryan Orange in 96 and 97).

Richard D'Ath becomes one of only two people to have won Easters an incredible three times - in 2008, 2009 and now 2010. Only Anna Adams from Auckland, in the early 1990s, has achieved such a feat. He's also one of only a few people to twice win the Dame Cath Tizard Cup for the best speaker.

Udayan's achievements are also noteworthy - the Most Promising Speaker at Joynt Scroll last year is now the Captain of the NZU Impromptu Team!
Finally, Seb Templeton has now won the last four traditional NZ domestic tournaments - surely something that has not occurred before. He has simultaneously held for two years now, the Joynt Scroll (2008), the Officers Cup (2009), Joynt Scroll (2009) and now the Officers Cup (2010).

Go Vic!
