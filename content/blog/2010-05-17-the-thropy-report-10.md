---
layout: blog
title: The Thropy Report '10
publishDate: 2010-05-17
---
Once again Vic has come back from Thropy tired but full of joy and excitement and plans for Palmy North 2010 and Ahmed Zhaoui's new kebab stand in the PN square. Vic sent a very strong squad, and broke two teams. Julia Wells, Nicola Wood, Cassandra Shih broke first as they only unbeaten team in the rounds, and Asher Emanuel, Jasmin Moran, Jodie O'Neill broke third. Unfortunately both teams lost in close semi-finals, giving Auckland two teams in the final.

On the individual front the squad had huge success, including taking three of the top four speaker awards. Asher Emanuel blew people away in his winning of the Chalice of the Future for the Best Speaker at Thropy. He and Jodie O'Neill were named in the North Island Debating Team, and Campbell Herbert was the first reserve to that team. Julia Wells and Jasmin Moran won Highly Commended speaker awards.

In terms of judging Seb Templeton, Clodagh O'Connor-McKenna, Hugh McCaffrey, Richard D'Ath and Ihaia Tichborne all broke to the semi-finals in a very competitive judges break. Seb, Clodagh and Hugh judged the Grand Final also. Philip Belesky had a fantastic tournament as a trainee became an officially qualified NZUDC accredited adjudicator with ease. The other Vic trainees performed very well, and some came extremely close to accreditation.

While sad to lose our beloved Thropy, the squad should be proud of themselves. The Victoria competitors were outstanding and the break and speaker prizes made it clear that while Auckland have the Thropy for this year, it was an extremely close run thing. Their persistence has paid off, and there was always going to be an inevitable Auckland win some time very soon. The strength of our squad says great things about our level of adjudication skill, and bodes well looking forward to some great novices entering Easters and Joynt Scroll teams in the near future.

A massive, massive thank you goes out to Ihaia Tichborne for leading the squad and sorting out everything; to Seb Templeton for chief adjudicating the tournament; and to Johnny Crawford, Campbell Herbert, Jodie O'Neill, Ashleigh Bennett, Emma Smith, Alex Sinclair, and Hugh McCaffrey for driving us to the 'Tron.
