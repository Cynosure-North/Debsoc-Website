---
layout: blog
title: Australs Squad Updated
publishDate: 2010-05-28
---
Massive thanks go to our selectors - Jono Orpin, Clodagh O'Connor-McKenna, Gareth Richards, Joe Connell, Polly Higbee, Josh Cameron. We now have a full and final squad of six teams, five judges:

__Vic 1__

Udayan Mukherjee

Stephen Whittington

Ella Edginton

__Vic 2__

Seb Templeton

Richard D'Ath

Paul Smith

__Vic 3__

Jenna Raeburn

Hugh McCaffrey

Holly Jenkins

__Vic 4__

Nick Cross

Asher Emanuel

Jodie O'Neill

__Vic 5__

Ihaia Tichborne

Sam Ward

Daniel Wilson

__Vic 6__

Aidan Beckett

Elisha Hsao

Brent Perry

__Judges__

Clodagh O'Connor-McKenna

Chamanthie Sinhalage

Cameron Harper

Prin Moodley

Channy Mao
