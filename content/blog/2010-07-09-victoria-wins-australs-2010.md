---
layout: blog
title: Victoria Wins Australs 2010
publishDate: 2010-07-09
---
On Wednesday night in Auckland at the SkyCity Theatre, Victoria won its fourth ever Australasian Intervarsity Debating title and its first since 1998.

Vic reached the Grand Final of Australs in 2007 and 2009, narrowly losing each year. 2010 was different, with Victoria recording a resounding 8-1 victory over Auckland 2, affirming that the ICC should be able to prosecute crimes against the earth.

Massive congratulations to the winning Victoria 1 team of Udayan Mukherjee, Stephen Whittington, and Ella Edginton. It was Stephen's third Australs Grand Final appearance in four years, and this time he did not walk away emptyhanded. Along with the Australs trophy Stephen also became the 2nd New Zealander to win the Jock Fanselow Cup for the best speaker in the Australs Grand Final. Stephen also ranked as the second best speaker of the entire tournament, just 0.5 of a mark behind the best speaker.

We also shouldn't forget the success of the Victoria Two team, who helped make this Victoria's best Australs performance ever. Victoria Two was knocked out by Victoria One in an amazing Vic-Vic semi-final (such events usually happen only at NZ-only tournaments!). Congrats to Seb Templeton, Richard D'Ath, and Paul Smith. Special congrats to Seb Templeton, who ranked as the 7th equal best speaker of the tournament.

Every other Vic team did well as well. Victoria Three (Jenna Raeburn, Hugh McCaffrey and Holly Jenkins) ranked 24th, missing out on breaking only on speaker points. Victoria Four (Asher Emanuel, Nick Cross and Jodie O'Neill), all at their first Australs, finished on four wins and threatened some big name teams along the way. Victoria 5 and 6 also finished on four wins each as well.

This Australs victory in many ways is the culmination of five years of steady improvement by Victoria at Australs - from the quarters in 2005, to the heart-breaking losses in the Grand Finals of 2007 and 2009, and now finally a well-earned victory in 2010. The future looks very bright indeed, with Australs champions Udayan and Ella only in their third year of study, semi-finalists Paul Smith and Richard D'Ath in their fourth year, and a crop of talened new first years coming through as well.
