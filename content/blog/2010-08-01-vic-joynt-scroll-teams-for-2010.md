---
layout: blog
title: Vic Joynt Scroll Teams for 2010
publishDate: 2010-08-01
---
Here are Vic's Joynt Scroll Teams for 2010. Congrats to all who made teams.

__Vic 1__: Jenna Raeburn, Udayan Mukherjee, Paul Smith

__Vic 2__: Asher Emanuel, Richard D’Ath, Nick Cross

__Vic 3__: Holly Jenkins, Tom Mathews, Jodie O’Neill

__Vic 4__: Emma Smith, Alex Sinclair, Julia Wells

Reserves: Daniel Wilson, Thomas McKenzie
