---
layout: blog
title: Vic Wins Joynt Scroll 2010
publishDate: 2010-09-02
---
On Monday in the Legislative Council Chamber of Parliament, Victoria won the Joynt Scroll - the most prestigious prize in NZ university debating - for the 5th year in a row. No other university has ever retained the famed shield for such a long period of time.

It was an all-Victoria final, with Victoria 2 (Asher Emanuel, Richard D'Ath and Nick Cross) narrowly beating Victoria 1 (Jenna Raeburn, Udayan Mukherjee, and Paul Smith) in a 4-3 split decision. It is the second all-Vic Joynt Scroll final in three years.

Victoria dominated the speaker tab as well, with all six speakers in the top two Vic teams appearing in the top ten speaker list of the tournament. Richard D'Ath topped the tab, winning the President's Cup for the best speaker of the tournament, which will sit on his mantelpiece alongside the Dame Cath Cup he won earlier in the year for being the best speaker at Easters. Richard is the first person to hold both cups simultaneously since Polly Higbee in 2006/2007. It is the fifth time in a row that Vic has won the President's Cup. Former winners can be found here.

Richard and Udayan were also named as members of the NZU Prepared Debating Team, with Richard being named captain. Richard and Udayan are also members of the NZU Impromptu Team.

Asher Emanuel and Paul Smith picked up highly commended prizes. Asher also picked up the newly-awarded unofficial "Whittington Cup" for a novice speaker who is ineligible for the Most Promising Cup award (by winning a highly commended - or higher - prize.)

Victoria has now won every major domestic debating tournament since April 2007 and holds the cups for impromptu, parliamentary and prepared debating.
