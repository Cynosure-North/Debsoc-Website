---
layout: blog
title: Stephen Whittington wins Victoria University Sportsperson of the Year
publishDate: 2010-10-06
---
In a result that will undoubtedly annoy the Rugby Club, the Rugby League Club...and, well, pretty much all physical sports clubs on campus, Stephen Whittington was tonight named the Victoria University of Wellington Sportsperson of the Year. Stephen is the second debater to win the title in the last four years.

Stephen's achievements in 2009/10 include winning the Australasian Intervarsity Debating Champs (Vic's first win since 1998), ranking as the second best speaker at the tournament, reaching the octo-finals at the 2010 World Universities' Debating Champs, and finishing as a runner-up and top 10 speaker at the NZ British Parliamentary Debating Champs.

Eight other Vic debaters won blues: Australs winners Udayan Mukherjee and Ella Edginton, Easters winners Seb Templeton and Richard D'Ath, Australs semi-finalist Paul Smith, and Worlds octo-finalist Polly Higbee.

Seb Templeton and Sarah Wilson also win blues for administration, with Seb Templeton being named the Sports Administrator of the Year for his good work leading Debsoc this year, along with running the Sports Council.
