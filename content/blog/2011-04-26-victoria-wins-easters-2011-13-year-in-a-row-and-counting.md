---
layout: blog
title: Victoria wins Easters 2011 - 13 year in a row and counting!
publishDate: 2011-04-26
---
Even the curse of the unlucky 13 could not stop Victoria's domination of the 2011 University Games debating tournament (Easters) held in Auckland over the Easter weekend.

Leading the charge to win the tournament for a 13th year in a row for Vic were Victoria One (Udayan Mukherjee and Ella Edginton) and Victoria Two (Asher Emanuel and Paul Smith).

Victoria Two finished the preliminary rounds in second place, with six wins from seven (which included affirming 6 times). Victoria One were one spot further back in third place, having collected five wins.

This set up a Victoria v Victoria semi-final, guaranteeing Victoria a team in the Grand Final. The semi was a cracker of a debate, with Victoria Two eventually prevailing in a 4-1 split decision, affirming "That third parties should be prosecuted for failing to report domestic abuse".

The Grand Final was a nerve-wracking affair, but Asher and Paul did Vic proud, winning 5-2 after negating "This House opposes a free trade agreement with the USA". The Officers' Cup is staying in Wellington for yet another year!

Vic cleaned up in the personal awards as well. Asher Emanuel and Paul Smith were named as the NZ Universities' Impromptu Debating team, with Ella Edginton as the first reserve and Udayan Mukherjee as the second reserve. Asher won the Dame Cath Tizard Cup as the best speaker of the tournament, and was also named as captain of the NZU Team. Jodie O'Neill received a promising speaker award, good reward for a successful tournament as part of the Vic Three team.

On that note, it's worth noting and congratulating Victoria's other three teams, all of whom did very well as well. Victoria Three (Jodie O'Neill and Duncan McLachlan) finished 5th, missing out on the break by only a couple of speaker points. Victoria Four (Nick Cross and Aric Shakur) finished 8th, on four wins, and Victoria Five (Cameron Price and Johanna McDavitt) finished 12th, also on four wins. That means every Vic team finished in the top half of the draw - a great achievement. With the bulk of the debaters in Vic 3 through 5 being first and second years, the future for Vic is very bright indeed.

On the adjudicatorial front, Stephen Whittington won the Centennial Cup for best adjudicator, and also judged the final along with Seb Templeton (the Chief Adjudicator) and Richard D'Ath (a NZU selector). Christopher Bishop, Jenna Raeburn and Hugh McCaffrey judged the semi-finals. Finally, all three of Vic's trainees accredited - something that hasn't happened for a long time. Well done to Campbell Herbert, Emma Smith and Olivia Hall.
