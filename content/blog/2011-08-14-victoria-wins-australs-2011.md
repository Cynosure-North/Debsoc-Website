---
layout: blog
title: Victoria Wins Australs 2011
publishDate: 2011-08-14
---
In one of the greatest days in the long 112 year history of the Victoria University of Wellington Debating Society, Vic has defended its Australasian Intervarsity Debating Championships title, winning the final 7-2 over the National University of Singapore at the largest ever Australs held.

It was a monumental performance by Debsoc President Seb Templeton, Richard D'Ath and returning champion Udayan Mukherjee. They won 6 from 8 debates in the preliminary rounds, breaking in 4th place. In the octo-finals they knocked over UT Mara before defeating the highly-fancied Sydney 2 in the quarter-finals. In the semi-finals they knocked off the previously unbeaten and top-breaking Monash 1.

Victoria debaters past and present all around New Zealand watched the final on Wednesday night via the live webstream the Korea organisers had set up. In a fortuitous twist of timing, the final took place during the middle of the traditional Wednesday night Debsoc meeting. After a great debate and an agonising wait for the results, Victoria was crowned Australs 2011 Champions! Facebook and Twitter went mad.

There was extra honour for Udayan - he joins a very exclusive club of people to win Australs twice (he's only the second New Zealander), plus he won the Jock Fanselow Cup for the best speaker in the Grand Final after a stonkin' third negative speech.

Seb Templeton was named as the 3rd equal best speaker of the tournament, rich reward for a superb tournament where he led from the front for the Society. Richard D'Ath was unluckily ranked just outside the top 10

The Victoria Two team of Asher Emanuel, Paul Smith and Holly Jenkins also performed very well, breaking in 16th place and defeating Melbourne 1 in the double octo-finals before bowing out in the octo-finals to Monash. We're still waiting on the speaker tab but we've no doubt they'll be high up.

Victoria Three also did amazingly well, winning 5 from 8 debates and missing out on the break on speaker points. With two of the members of this team only in their first year, the future looks very bright.

And finally, Daniel Wilson was given the honour judging the ESL Grand Final. Great work.

It's worth reflecting on the following amazing stats:
 - This is Victoria's fifth Australs victory since 1975, and the second in a row
 - The last university to win back-to-back Australs was Monash in 2000/2001.
 - Victoria has now reached the Grand Final of Australs in four of the last five years - an achievement no other university can boast of
 - Victoria has reached the quarter-finals at every Australs since 2005
 - The last time a Vic team lost in the semi-finals, it was to another Vic team
 - Udayan Mukherjee is the only New Zealander to ever win back-to-back Australs and one of two people to ever win it twice
 - The Jock Fanselow cup has been won by three New Zealanders in its six year history (all Victoria debaters)
