---
layout: blog
title: Welcome to Debsoc's new website [Old]
publishDate: 2012-09-12
---
After several years, several campaign promises, and some nefarious domain-name trickery, Debsoc now has a new website. No more jagged logos, newspaper collages, and Wordpress!

With most of news updates going to Facebook and Twitter, the new site functions more as a historical archive and collection of debating resources. We're still working through the process of entering in all the data, but be sure to have a look around our records by person, award, or event.
