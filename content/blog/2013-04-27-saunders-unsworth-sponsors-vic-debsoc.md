---
layout: blog
title: Saunders Unsworth sponsors Vic Debsoc
publishDate: 2013-04-27
---
Saunders Unsworth has sponsored the Victoria University Debating Society since April 2013. Saunders Unsworth are New Zealand's foremost government relations consultancy, and are located in the heart of New Zealand's decision-making capital. We are thankful for their involvement and support. For more information on Saunders Unsworth head to [sul.co.nz](sul.co.nz).
