---
layout: blog
title: Vic Wins Easters for 2015!
publishDate: 2015-04-08
---
Vic Debsoc is proud to announce that we have won Easters for the 17th year in a row! Congratulations go to Nick Cross and Nick Gavey for this excellent achievement, who faced Auckland 1 in the final, with the motion "THBT comedians should not use racist, sexist, or homophobic language, even for the purposes of satire".

We would also like to congratulate Nick Gavey for achieving best speaker, Nick Gavey and Nick Cross for making the NZ Impromptu Team, Maddy Nash for achieving most promising speaker, Liam Dennis and Harrison Fookes for achieving promising speaker awards and James Gavey for accrediting as a judge.

Also special mentions go to Jodie O'Neill for fixing the tab system, Tamara Jenkin for making it to a semi-final as a trainee, Kimberley Savill for being an incredible president and making sure everyone made it to debates on time, and to all debaters, judges and trainees for making the tournament such an incredible experience.

This is a great achievement for all of Debsoc, whether you were at the tournament or not, it was great to see all the support, encouragement and dedication that goes in to making the society so successful pay off.
