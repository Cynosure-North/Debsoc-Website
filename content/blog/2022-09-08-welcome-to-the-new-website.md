---
layout: blog
title: Welcome to the new website
publishDate: 2022-09-08
---
New and shiny, this one should last another two decades. If you’re the person updating it again, Hello!

I’ve migrated everything from the old website so anything uploaded from 2023 onwards is all new content.
- North✨
