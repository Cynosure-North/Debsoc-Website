---
layout: blog
title: Officers 2023 Squad
publishDate: 2023-07-12
---
__V1__: Lucy, Peter

__V2__: Sophie, Cara

__V3__: Ethan, North

__V4__: Aidan, Peter

__V5__: Rebecca, Emma

__V6__: Gabe, Ollie

__Reserve__: Luke
