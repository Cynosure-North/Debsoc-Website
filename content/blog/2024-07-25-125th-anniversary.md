---
layout: blog
title: 125th Anniversary
publishDate: 2024-07-26
---
Happy 125 years of Victoria Debating Society! Last week we had the 125th Anniversary of the society at Parliament which was proudly sponsored by Meredith Connell ([MC](https://www.linkedin.com/company/meredith-connell/))! Thank you so much to MC for helping create such a spectacular event and we hope you enjoyed it too. We'd like to thank the Hon Chris Bishop for being our host MP for the venue.

We were greeted by many alumni from various generations who also came to celebrate and see old friends. We hope you enjoyed the event and we hope to see you in 25 years for the 150th.

![](/uploads/1721625026406.jpg)

![](/uploads/1721625023099.jpg)

![](/uploads/1721625024387.jpg)

![](/uploads/1721625025665.jpg)

![](/uploads/1721625026771.jpg)

![](/uploads/1721625027625.jpg)

![](/uploads/1721625026874.jpg)

![](/uploads/1721625020718.jpg)
