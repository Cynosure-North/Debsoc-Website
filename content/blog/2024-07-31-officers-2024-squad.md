---
layout: blog
title: Officers 2024 Squad
publishDate: 2024-07-31
---
**V1:** Lucy, Peter

**V2:** Sophie, Emma

**V3:** Cara, Jack

**V4:** Peter, Isabella

**V5:** Rebecca, Xandi

**V6:** Hannah, Liv

**V7:** Saad, Will

**Trainees:** North, Ollie

**Judges:** Keir, Brontë, Ollie, Ethan
