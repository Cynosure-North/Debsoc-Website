---
layout: event
title: 125th Anniversary proudy sponsored by Meredith Connell (MC)
draft: false
publishDate: 2024-05-18
summary: Our society was formed in 1899, join us to celebrate our 125th anniversary.
startDate: 2024-07-19T18:00:32+1200
endDate: 2024-07-19T21:00:00+1200
region: Wellington
location: Grand Hall, Parliament
---
Our society was formed in 1899, join us to celebrate our 125th anniversary. 

There will be a dinner, speeches, and a show debate.

{{< blank-line >}}

Thanks to Meredith Connell (MC) for sponsoring this event.

Thanks to Chris Bishop for hosting us in parliament.


{{< blank-line >}}


Dress code: buisness formal

Cost for alumni: $50

Please RSVP: contact@vicdebsoc.org.nz




