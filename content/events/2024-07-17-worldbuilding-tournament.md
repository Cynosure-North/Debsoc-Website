---
layout: event
title: Worldbuilding Tournament
draft: false
publishDate: 2024-07-18
summary: Bring your hard hats
startDate: 2024-07-27T09:00:30+1200
endDate: 2024-07-27T19:00:00+1200
region: Wellington
location: Rutherford House
---
[Link to facebook page](https://facebook.com/share/fvv9qhlvewtauund)
