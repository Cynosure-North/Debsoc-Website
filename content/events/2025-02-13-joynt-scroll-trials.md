---
layout: event
title: Joynt Scroll Trials
draft: false
publishDate: 2025-02-13
summary: Joynt Trials are being held at 6pm on the 4th of march
startDate: 2025-03-04T18:00:00+1300
endDate: 2025-03-04T22:00:00+1300
region: Wellington
location: Rutherford House
---
We are holding our trials for Joynt at 6pm on Tuesday the 4th of March, at Rutherford House. Teams are selected by a range of wonderful people so there is no need to sign up with a team. Please register by Monday 3rd of March at 5pm.
