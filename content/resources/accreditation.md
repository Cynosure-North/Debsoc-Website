---
layout: resource
title: Accreditation
cover_image: /uploads/edvard_munch_-_foster_mothers_in_court.jpg
draft: false
publishDate: 2024-04-28
summary: New Zealand's accreditation process is a unique system that means we
  have some of the best judges in the world.
---
As a trainee you will be assigned to judge under an accredited judge. You will judge the debate, and after the teams have left the room you will deliver your OA to the judge. They will ask you questions to assess your understanding of the debate. If your OA was similar to theirs and high quality, they will invite you to deliver it to the teams, perhaps with some small tweaks. It's ok if you disagree with their result or why you came to the decision, judges are looking for how well you explain your reasoning, although they will give the OA if you disagree. Usually trainees also give feedback to the speakers.

After the round the judges will all go to the judge room. There the judges will discuss your OA, how you saw the debate, and how well you have been doing so far. They will also discuss any feedback they gave you to to see that you're able to apply feedback and improve round to round. Adj Core will use this to place you in rooms for the next round.

After the in rounds, if you've had universally excellent OAs your accreditation will be announced at break night. More likely though, is that you will be called back to trainee the semi finals. If you do well in those you will progress to trainee the finals. If you accredit after the finals your accreditation will be announced at prizegiving.

To officially accredit, two supers from different campuses need to agree to accredit you. Supers are people who have been appointed to an adj core of a major tournament before - which is Joynt Scroll, Officer's Cup, Australs, and Worlds. The standard supers look for is breaking quality judges. If they judge you up to the standard they will accredit you.

Accreditation doesn't expire, but super status does after 5 years. 

You can accredit through the trainee process at these tournaments:

* Joynt
* Officers
* Thropy
* WGM
* Claytons (South Island Novice Tournament, Vic doesn't attend)

It is also possible to accredit via the break. This means breaking as a speaker at 2 different majors, domestic or international. Once that happens you will be instantly accredited

You can accredit via the break by breaking at these tournaments:

* Joynt
* Officers
* Australs
* AusWGM
* Worlds
* (Probably others but these are the only ones Vic regularly attends)
