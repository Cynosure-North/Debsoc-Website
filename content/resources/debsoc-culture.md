---
layout: resource
title: Debsoc Culture
cover_image: /uploads/slide4.jpg
publishDate: 2023-11-09
summary: A record of the culture of debsoc, as far as records go.
---
[Victoria ](https://www.youtube.com/watch?v=fmzLGVGZkVQ)by the Dance Exponents has been the traditional Debsoc song for many yeas now. Whether played from a cellphone marching along Christchurch streets at Easters, or requested for the band at Australs 2006 in the Duxton, or again at Australs 2012 in the Duxton, this song is one for rousing the spirits of the society.

___

Intrepid debaters of the early 2000s created 4 mixtapes, known as the “Here’s What I Call Banter” series. The (partially recovered) playlist is available [here](https://open.spotify.com/playlist/4gWEpxUclb3SgWFtNNQDyT?si=982bd74a25f9436a).

___

Squirrel was a pentannual publication detailing all the goings on of debsoc in the 2010s. The archive is available [here](https://drive.google.com/drive/folders/1vZwTNKZECGsnprHgJWVrNx284ym63qaD?usp=sharing).

___

Various sub-clubs have existed at different times. Some of them have a secret passphrase. They include:

The Vic Debosc Ski Society

The Craft Club

The Other Craft Club

The Better Film Society

Some people who play board games sometimes

weird music appreciators

The Secret² Society

Bristol Exec

The 59ers
