---
layout: resource
title: Guide to British Parliamentary Format
cover_image: /uploads/slide2.jpg
publishDate: 2023-11-09
summary: British Parliamentary (BP) is the most challenging and unique style. It
  is the format that NZBP and Worlds use.
---
Also known as BP.

---

In a BP debate there are four teams with two members each. Two teams are on the government side and two on the oppositon side. One team on each side is the opening team for their side, and the other is the closing. The teams are randomly given a position – OG, OO, CG or CO. In tournaments these will try to be evened out when possible within given debating match-ups. A topic is then announced and the teams are given 15 minutes’ preparation time for the debate. The OG team are allowed to prepare in the room set aside for the debate.

The names of the different speakers in the debate are as follows:

**Opening Government** (OG)

- Prime Minister (PM)
- Deputy Prime Minister (DPM)

**Opening Opposition** (OO)

- Leader of the Opposition (LO)
- Deputy Leader of the Opposition (DLO)

**Closing Government** (CG)

- Government Member / Member of Parliament (GM/MP)
- Government Whip (GW)

**Closing Opposition** (CO)

- Opposition Member / Member of the Opposition (OM/MO)
- Opposition Whip (OW)

After the 15 minutes’ preparation time the Prime Minister is asked to open the debate.
Following the pattern of all styles the speaking order is: PM, LO, DPM, DLO, GM, OM, GW, OW.

Speeches are from 6-7 minutes with one bell after 1 minute, another one after 6 minutes, and two at 7 minutes. Between the first two bells points of information may be offered if the current speaker is from the other side of the house (as in not just another team but actually the other side). POIs are extremely important in BP debating.
In BP style debating there are no Leader’s replies. The debate ends after the Opposition Whip’s speech has ended.

In BP debating, although teams share their side with another team, they are all in competition. Thus the teams are ranked 1st to 4th at the end of the debate.

An opening team in BP must do a similar job to in normal NZ debating. They must offer a case or a model (OG) or attempt to dismantle a case or model (OO). The key difference is in staying in the debate. Opening teams are marked highly when the issues which they raised are prevalent through the whole debate, not just the opening half. They are also marked on how well they stay in the debate through using POIs later in the debate to show the importance of arguments they have raised, or to demonstrate that they could have easily dealt with the arguments raised in the closing half of the debate.

The closing teams must both follow team lines consistent with their respective closing teams. If they do not it can be seen as “knifing” and look bad for a team. However the closing teams are in competition with their opening teams. They are expected to provide an extension to the debate – a new area of argumentation or sufficiently new analysis on issues already brought up. A closing team should still try to prove their case or dismantle the Governments, but should also try to do so in a sufficiently different or more effective manner than their opening team. The closing teams are also expected to summarise the debate. This is done in the two whip speeches and should frame the debate to attempt to show the importance of the extension or new analysis that was provided.
