---
layout: resource
title: Guide to Impromptu Format
cover_image: /uploads/there-is-no-slide-3.jpg
publishDate: 2023-11-09
summary: Impromptu debates are known for being quick and a lot of fun.
---
Also known as Officers', or Easters.

___

In an Officers debate there are two teams – the traditional affirmative and negative. Each side has two speakers each, and speaking order is the same as Australs, except with no third speaker.

The adjudicator will have two moots that are concealed from the debaters, and a stopwatch to watch over the preparation period.

A coin is tossed. Whoever wins the coin toss is allowed to choose either:

Topic – the team is shown the two moots privately, and has one minute to decide which they would like to debate.

Side – the other team is shown the two moots, and has one minute to decide which they would like.

The topic is then announced to the other team. Whichever team chose the topic then has to wait for one minute while the other team chooses which side they would like to debate – either affirming (and therefore speaking first) or negating the topic.

Prep time is 5 minutes. Speeches are 6 minutes, with no POIs. Leader’s replies are 3 minutes, and must be given by the first speaker.
