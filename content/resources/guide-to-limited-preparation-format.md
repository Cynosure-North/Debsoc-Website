---
layout: resource
title: Guide to Limited Preparation Format
cover_image: /uploads/slide1.jpg
publishDate: 2023-11-09
summary: Limited Prep debating is the most common in New Zealand. It's the format used in schools-level debating.
---
Also known as Schools, Joynt, or Asian Parliamentary.

___

In a limited preparation debate there is an affirmative and a negative team, each with three speakers. Three potential moots are kept secret by an official.

1. An official starts timing 30 minutes and the three topics are displayed in a place where both teams can see them.
1. Each team ranks the three moots from 1 to 3 on which they would rather debate. 1 is the preferred choice.
1. The two teams come together and compare rankings. The moot(s) that each team ranked 3rd are automatically taken out, they will not be debated.
If there is only one moot remaining: That is the one which is debated.
If there are two moots left, and both teams have the same 1st: The 1st ranked topic by both teams is debated.
Otherwise: A coin is flipped to choose which team gets to use their 1st ranked moot.
1. The teams then leave to prepare for the debate for the remainder of the 30 minutes being timed. The affirmative team is allowed to prepare in the designated room of the debate. The negative team must find somewhere else to prepare (usually in the hall).
1. After the 30 minutes is up the teams are summoned and the first affirmative speaker is called upon to begin the debate.

Speeches are from 6-8 minutes, with points of information allowed from 1-7 minutes. One bell is given at 1 minutes, one bell at 7 minutes, and 2 at 8 minutes. As with impromptu debating, the affirmative speaker of each position goes before the negative speaker of that same position.

Each team is given a 3-4 minute Leader’s reply, with one bell at 3 minutes and two at 4 minutes. Only the 1st or 2nd speakers of either team may give this reply. As with all styles which have replies, the negative reply goes first, immediately following the 3rd negative speech. The affirmative reply ends the debate.

# Australs

Australs debates are as above, except that there are no POIs.