---
layout: resource
title: Hall of Fame
cover_image: /uploads/1600px-alexander_iii_and_maria_fedorovna-s_coronation_by_g.becker.jpg
draft: false
publishDate: 2023-11-09
summary: Vic Debsoc has a long and incredibly successful history. Here we
  remember our most successful members.
---
{{< contents >}}

# New Zealand Tournaments

## Joynt Scroll (New Zealand Universities' Limited Preparation Debating Championships)

### I. Winners of Joynt Scroll

*The University of New Zealand Debating Challenge Shield*\
*Presented by JW Joynt esq. MA*

S. Penno, A. Leslie, O.E Battell-Wallace — 2018\
N.W Cross, O. Hall, C. Price — 2013\
A.G Emanuel, J.J O’Neill, U. Mukherjee — 2012\
N.W Cross, D.F McLachlan, H.H Jenkins — 2011\
A.G Emanuel, R.J D’Ath, N.W Cross — 2010\
S.J Templeton, S.A Whittington, E.T.V Edginton — 2009\
K.L Errington, J.C Pring & S.J Templeton — 2008\
C.J.S Bishop, J.S Connell & S.A Whittington — 2007\
H.A McCaffrey, P.H Higbee & L.J Holden — 2006\
C.J.S Bishop, R. Clouston & S. Islam — 2003\
K.A.J Moar, N.V Willis & D. Small — 2002\
A. Livesey, C. Payne & D. Small — 2000\
D.R Orange, H.M Finlay & B.J Gandy — 1997\
Miss C.M Williams, N.C Miller & B. Cash — 1994\
Miss C.M Williams, N.C Miller & Miss A.J Gillespie — 1993\
A.M Crawley, A.E Howman & D. Capie — 1988\
I.R Rennie, J.R King & D.I Stevens — 1985\
N.J.A Sainsbury, J.R King & D.I Stevens — 1984\
P.F Biggs, J.R Allen & D.I Stevens — 1982\
N.J.A Sainsbury, Ms M.D McCallum & Miss B.D Golder — 1981\
R.P Baker, P.M Ridley-Smith & R.K Stephenson — 1980\
D. Linney, S. Kos & R.P Baker — 1978\
C.D Falconer BA (Hons), D. Linney & R.P Baker — 1977\
C.D Falconer BA (Hons), D. Linney & V. Goldblatt MA (Hons) — 1976\
T.J Groser BA, P.F Boshier & R.A Campbell BA — 1972\
H.P Stubbs, J.G Blincoe & E.R Fairbrother — 1971\
H.S Hancock BA, H.P Stubbs & P.P Butler — 1970\
Miss B.A Foot, H.P Stubbs & B.R Newell — 1969\
J.C.M Rose BSc, G.P Curry & S.A Whitehouse — 1966\
P.J.R Blizard BA (Hons), D.A Shand & A.H Ashenden BCom — 1965\
P.J.R Blizard BA & J.B McKinley — 1964\
T.A Roberts & F.A Hamlin — 1961\
E.W Thomas LLB & J.M Whitta BCom — 1958\
Miss H. O'Flynn & K.T Fowler — 1943\
W. Wah & Miss B. Hutchinson — 1942\
P.J Sheehan & H.C Bowyer — 1940\
R.W Edgely & R.L Meek — 1939\
A.R Perry LLM & S.G Andrews MA — 1937\
H.R Bannister & Miss C.S Forde — 1932\
W.J Mountjoy & H.R Bannister — 1931\
J.F Platts-Mills & W.P Rollings — 1928\
S.E Baume & R.M Campbell — 1925\
J.W.C Davidson & F.H Haig — 1923\
E. Evans & W.E Leicester — 1919\
I.M Moss & L.P Leary — 1915\
G.G.G Watson MA/LLB & A.B Sievwright — 1914\
F.G Hall Jones BA & G.W Morice MA — 1912\
C.H Taylor MA & M.H Oram MA — 1911\
D.S Smith & G.H Gibb — 1909\
J. Mason & H.E Evans — 1908\
B.E Murphy BA & H.F O'Leary — 1907\
E.J Fitzgibbon & F.P Kelly — 1906  

### II. The President’s Cup

*Presented to the Best Speaker at Joynt Scroll*\
*Donated by Dr Andrew Stockley, President of the New Zealand Universities Debating' Council, 1985-1988.*

Emily Partridge — 2020\
Shakked Noy — 2019\
Shakked Noy — 2018\
Udayan Mukherjee — 2012\
Paul Smith — 2011\
Richard D’Ath — 2010\
Stephen Whittington — 2009\
Kathy Scott-Dowell — 2008\
Joe Connell — 2007\
Polly Higbee — 2006\
Christopher Bishop — 2004\
Nicola Willis — 2001\
Duncan Small — 2000\
Bryn Gandy — 1998\
Ryan Orange — 1997\
Ryan Orange — 1995\
Catherine Williams — 1994\
Neil Miller — 1993  

### III. The Bledisloe Award for Oratory

*Presented to the winner of the prepared oratory competition held as part of Joynt Scroll.*

L.J Holden — 2006\
C.J.S Bishop — 2005\
G. Cameron — 1998\
B.G.A Morris — 1997\
K. Pownall — 1995\
Miss S. Marks — 1993\
D. Capie — 1991\
A. Stockley — 1987\
P. Sainsbury — 1986\
N.J.A Sainsbury — 1984\
P. Biggs — 1981\
Miss K. Gunn — 1980\
D.L Hutchinson — 1971\
J.B Robertson — 1968\
Miss M. Boyle — 1962\
B.M O'Connor — 1947\
J.R McCreary — 1941\
J.B Aimers — 1938\
Miss C.S Forde — 1935\
K.H Melvin — 1932  

## Officers' Cup (New Zealand Universities' Impromptu Debating Championships)

### I. Winners of Easters/Officer’s Cup

*Presented to the winners of the Debating competition at the NZ University Games – “Easters”.*

Sam Penno & Sam O’Grady — 2019\
Shakked Noy & Maddy Nash — 2017\
Nick Gavey & Nick Cross — 2015\
Nick Gavey & Jodie O’Neill — 2014\
Asher Emanuel & Jodie O’Neill — 2013\
Asher Emanuel & Udayan Mukherjee — 2012\
Asher Emanuel & Paul Smith — 2011\
Seb Templeton & Richard D'Ath — 2010\
Seb Templeton & Richard D'Ath — 2009\
Polly Higbee & Richard D'Ath — 2008\
Yogesh Patel & Stephen Whittington — 2007\
Tom Fitzsimons & Stephen Whittington — 2006\
Christopher Bishop & Joe Connell — 2005\
Christopher Bishop & Gareth Richards — 2004\
Ranald Clouston & Sayeqa Islam — 2003\
Chelsea Payne & Nicola Willis — 2002\
Kevin Moar & Nicola Willis — 2001\
Chelsea Payne & Duncan Small — 2000\
Matt Cuthell & Angela Ballantyne — 1999\
Rob Salmond & Ryan Orange — 1997\
Rob Salmond & Ryan Orange — 1996\
M Hickey & Justine Munro — 1990  

### II. Dame Catherine Tizard Cup for the Best Speaker at Officers' Cup

*Presented to the Best Speaker at the Officers’ Cup tournament.*

Maddy Nash — 2018\
Maddy Nash — 2017\
Nick Gavey — 2015\
Jodie O’Neill — 2014\
Jodie O’Neill — 2013\
Asher Emanuel — 2011\
Richard D’Ath — 2010\
Richard D’Ath — 2009\
Polly Higbee — 2008\
Polly Higbee — 2007\
Joe Connell — 2006\
Nicola Willis — 2002\
Chelsea Payne — 2001\
Chelsea Payne — 2000\
Matt Cuthell — 1999\
Ryan Orange — 1997\
Robert Salmond — 1996  

### III. The Sir David Beattie Cup for Public Speaking

*Presented to the winner of the impromptu public speaking competition held as part of Officers’ Cup.*

Daniel Wilson — 2010\
Richard D’Ath — 2009\
Richard D'Ath — 2007\
Josh Cameron — 2006\
Foster J Sayers — 2004\
Neil Miller — 1991\
Justine Munro — 1990\
Mike Dorf — 1987\
Raybon Kan — 1986  

## NZ British Parliamentary Open Debating Champs (formerly Vice-Chancellor’s Cup)

### I. Winners of NZBP

*Presented to the winners of the NZ British Parliamentary Open Debating Champs.*\
*Donated by the Victoria University of Wellington Debating Society.*

Lucy Jessep & Peter Lang - 2022\
Ailidh Leslie & Amy Spittal — 2019\
Jenna Raeburn & Nick Cross — 2018\
Udayan Mukherjee & Shakked Noy — 2016\
Cameron Price & Richard D’Ath — 2014\
Seb Templeton & Richard D’Ath — 2012\
Udayan Mukherjee & Paul Smith — 2009\
Polly Higbee & Stephen Whittington — 2008\
Christopher Bishop & Gareth Richards — 2007\
Christopher Bishop & Gareth Richards — 2005\
Christopher Bishop & Gareth Richards — 2004  

### II. Best Speaker of NZBP / David Lange Cup

*Presented to the Best Speaker at the NZ British Parliamentary Open Debating Champs.*\
*Donated by the Victoria University of Wellington Debating Society.*

Ailidh Leslie — 2022\
Maddy Nash — 2020\
Ailidh Leslie — 2019\
Richard D’Ath — 2018\
Richard D’Ath — 2017\
Richard D’Ath — 2016\
Jodie O’Neill — 2015\
Stephen Whittington — 2014\
Asher Emanuel — 2013\
Stephen Whittington — 2010\
Gareth Richards — 2007\
Kevin Moar & Sayeqa Islam — 2006\
Sayeqa Islam — 2005\
Joe Connell — 2004  

## Other Tournaments and Awards

### I. Russell McVeagh Cup

*Presented to the Most Promising Speaker at Joynt Scroll or Officers’ Cup.*\
*Donated by Russell McVeagh in 2002.*

Shine Wu — April 2019\
Taran Molloy — September 2017 
Lucy Kenner — April 2016\
Maddy Nash — April 2015\
Cameron Price — August 2012\
Paul Smith — April 2010\
Udayan Mukherjee — August 2009\
Jenna Raeburn — August 2008\
Ella Edginton — April 2008\
Kathy Errington — August 2007\
Pei Huang — April 2006\
Clodagh O’Connor-McKenna — September 2005\
Josh Cameron — April 2004\
Christopher Bishop — September 2003\
Joe Connell — April 2003  

### II. McLeod Trophy for North Island University Novice Debating

*Presented to the winners of the North Island Novice University Debating Competition (Thropy).*\
*In memory of Paul McLeod (1980-2002).*\
*Donated by the Victoria University of Wellington Debating Society.*

Dixie Vodka 400 (A. Muir, L. Jessep, I. Fraser) — 2021\
Victoria One (S. Penno, K. Robinson, B. Kinajil-Moran) — 2018\
Victoria One (J. Comer-Hudson, S.Noy, S. Mackenzie) — 2016\
Victoria Two (M. Jackson, O. Battell-Wallace, J. Gavey) — 2014\
Victoria One (A. Varney, B. Guerin, J. Lomax-Sawyers) — 2013\
Victoria One (A. Shakur, C. Price, D. McLachlan) — 2011\
Victoria Four (H. Jenkins, P. Smith, F. Ratner) — 2009\
Victoria Four (U. Mukherjee, T. Mathews, J. Pring) — 2008\
Victoria One (E. Bruce, K. Scott-Dowell, R. D’Ath) — 2007\
Victoria Two (C. Foulkes, I. Gordon-Smith, H. McCaffrey) — 2006\
Victoria One (M. Mabbett, M. Gledhill & T. Stokell) — 2002  

### III. Auckland Intervarsity Debating Championship

*Presented to the winners of the Auckland Intervarsity Debating Competition.*\
*Donated by Auckland Debating Society.*

Sam Penno — 2018\
Siobhan Davies — 2017\
Richard D’Ath — 2016\
Oscar Battell-Wallace — 2016\
Nick Cross — 2011  

### IV. The Sheppard Cup

*Presented to the winners of the NZ Women and Gender Minorites' tournament.*

Emilie Horsfall, Sophie Land, Bridget Scott — 2022\
A Brown, Izzy Fraser , Ailidh Leslie — 2021\
I Atchison, G Ellis, R Meagher, Maddy Nash — 2019\
Ailidh Leslie, E Richardson, H Thompson — 2018  

### V. Centennial Trophy for the Best Adjudicator at a NZU Tournament

*Presented to the Best Adjudicator at Joynt Scroll or Officers’ Cup.*\
*Donated by the Victoria University of Wellington Debating Society during the centenary year of the Society in 1999.*

Emily Partridge — Jan 2022\
Emily Partridge — April 2021\
Oscar Battell-Wallace — April 2019\
Richard D’Ath — April 2018\
Scott Fletcher — April 2017\
Jodie O’Neill — September 2013\
Udayan Mukherjee — April 2013\
Richard D’Ath — August 2012\
Seb Templeton — April 2012\
Seb Templeton — August 2011\
Stephen Whittington — April 2011\
Yogesh Patel — April 2010\
Gareth Richards — August 2008\
Joe Connell — April 2007\
Joe Connell — August 2006\
Gareth Richards — April 2006\
Christopher Bishop — August 2005\
Sayeqa Islam — April 2004\
Sarah Barnett — August 2003\
Kevin Moar — April 2003\
Bryn Gandy — August 2001\
Hamish Finlay — April 2000\
Neil Miller — August 1999  

### VI. The Grand Slam

*People who have won Officers’ Cup, Joynt Scroll, and the NZ BP Open Debating Champs.*

Richard D’Ath — Officers’ Cup 2009, Joynt Scroll 2010, NZ BP 2012\
Seb Templeton — Joynt Scroll 2008, Officers’ Cup 2009, NZ BP 2012\
Udayan Mukherjee — NZ BP 2009, Officers’ Cup 2012, Joynt Scroll 2012\
Polly Higbee — Joynt Scroll 2006, Officers’ Cup 2008, NZ BP 2008\
Stephen Whittington — Officers’ Cup 2006, Joynt Scroll 2007, NZ BP 2008\
Christopher Bishop — Officers’ Cup 2005, NZ BP 2005, Joynt Scroll 2007\
Christopher Bishop — Joynt Scroll 2003, Officers’ Cup 2004, NZ BP 2004  

# VUW Competitions

## Victoria Shield

*Presented to the winners of the impromptu debating competition run by the Wellington Speaking Union.*

\[Before 2000 known as Management Cup]

2019: Ailidh Leslie & Shakked Noy\
2017: Sam Penno & Jodie O’Neill\
2013: U. Mukherjee & Joe Connell\
2012: Hugh McCaffrey & Nupur Upadhyay\
2011: U. Mukherjee & E. Edginton\
2010: P. Smith & C. O’Connor-McKenna\
2008: G. Richards & Y. Patel\
2007: S. Whittington and S. Islam\
2006: S. Whittington and S. Islam\
2003: M. Sanders and E. Braunstein\
2002: P. Cornege and N. Miller\
2001: D. Small and C. Payne\
2000: H. Finlay and R. Salmond\
1999: H. Finlay and R. Salmond\
1998: H. Finlay and R. Orange\
1997: N. Miller and R. Salmond\
1996: N. Miller and R. Salmond\
1995: A. Gillespie and N. Miller\
1994: T. Thomson and S. Sampson\
1993: D. Capie and C. Walker\
1992: D. Capie and A. Crawley\
1991: D. Capie and A. Crawley\
1988: R. Kan and J. King\
1985: I.R Rennie and A.P Stockley\
1984: N. Sainsbury and J. King\
1982: J. Fanselow and J. Allen\
1981: T. O’Brien and J. Fanselow\
1978: L. O’Connor and T. O’Brien\
1977: D. Linney and C. Falconer\
1973: H. Stubbs and H. Hancock  

## Parliamentary Shield

*Presented to the winners of the Premier Grade adult debating competition run by the Wellington Speaking Union.*

2019: Richard D’Ath, John Brinsley-Pirie, Joe Ascroft\
2018: Tom O’Brien, Tim Bain, Richard D’Ath\
2017: Shakked Noy, Ailidh Leslie, Maddy Nash\
2016: Nick Cross, James Penn, Lucy Harrison\
2015: Richard D'Ath, Josh Baxter, Paul Smith\
2014: Asher Emanuel, Jodie O’Neill, Nick Cross\
2013: Richard D’Ath, Joe Connell, Kevin Moar\
2012: A.G Emanuel, R. J D’Ath, P. Smith\
2011: C J S Bishop, J J Baxter, S A Whittington\
2009: J B Orpin, S A Whittington, G M Richards\
2008: C.J.S Bishop, P.H Higbee, S.A Whittington, G.M Richards, M.F Mabbett\
2003: K.A.J Moar, M. Birdling, P.V Cornegé, M. Smith\
2001: S. Islam, R. Clouston, N. Willis, J. Zuccollo\
2000: A. Livesey, J. Clarke, A. Reid\
1999: H. Finlay, R. Salmond, B. Gandy, N. Miller\
1998: H. Finlay, R. Salmond, B. Gandy, R. Orange\
1997: N. Miller, H. Finlay, R. Salmond, R. Orange\
1995: C. Williams, B. Cash, M. Guest\
1994: C. Williams, B. Cash, M. Guest\
1993: C. Walker, S. Stubbs, D. Capie, P. McDonald\
1985: P.R.F Biggs, N.J Sainsbury, J.D.A Fanselow\
1960: F.A Hamlin, H. MacNeil, O. Tamasese  

## Plunket Medal for Oratory

2018: K. Robinson\
2017: S. Noy\
2016: L. Dennis\
2015: Miss S. Raisbeck\
2014: D. Wilson\
2013: Miss E. Smith\
2012: Miss J. O’Neill\
2011: Miss O. Hall\
2010: Miss S. Wilson\
2009: A. Kang\
2008: R.J D'Ath\
2007: R.W Allan\
2006: J.S Connell\
2005: R.D Christie\
2004: C. King\
2003: M.F Mabbett\
2000: R. Clouston\
1999: M. Cuthell\
1998: G. Cameron\
1997: B.G.A Morris\
1996: D.R Orange\
1995: C. Holland\
1994: R. Salmond\
1993: Miss C.M Williams\
1992: Miss A.J Gillespie\
1991: N.C Miller\
1990: Miss M.V Myers\
1989: P. Diamond\
1988: M.J Hickey\
1987: N.D.J Coyle\
1986: C.F Ryder\
1985: J.M.R Fowler\
1984: R.P Baker\
1983: Miss J. Wilson\
1982: N.J Sainsbury\
1981: J.C Roseveare\
1980: P.R Biggs\
1979: D.R Cotterall\
1978: L. O'Connor\
1977: Miss H. Sinclair\
1976: J. Bishop\
1975: P. Greene\
1974: R. Fairbrothe\
1973: Miss V. Mooney\
1972: B. Newell\
1971: D. L Hutchinson\
1970: H.S Hancock\
1969: Miss B.A Foot\
1968: S. Italio\
1967: P.P Butler\
1966: J.R Wareham\
1965: A.H Ashenden\
1964: P.J Blizard\
1963: A. Afeaki\
1962: T.A Roberts\
1961: F.A Hamlin\
1960: Miss M. Boyle\
1959: W. Waddell\
1958: J.M Whitta\
1957: W.D Dent\
1956: H.C MacNeil\
1955: G.N Cruden\
1954: B.M Brown\
1953: D.R Murray\
1952: C.V Bollinger\
1951: F.L Curtin\
1950: M.F McIntyre\
1949: H.J Brenda\
1948: A.J Williams\
1947: J.D Milburn\
1946: K.B O'Brien\
1945: K.L Neuberg\
1944: J.C.P Williams\
1943: H.M.B O'Leary\
1942: Miss M.C Crompton\
1941: L. Nathan\
1940: J.B Bergin\
1939: B.M O'Connor\
1938: W. Wah\
1937: A.H Scotney MA\
1936: J.B Aimers\
1935: K.J.T Tahiwi\
1934: R.J Larkin\
1933: A. Katz\
1932: Miss C.S Forde\
1931: Miss Z.R.M Henderson\
1930: A.E Hurley\
1929: A.D Priestley\
1928: W.J Mountjoy\
1927: W.P Pollinger\
1926: J.F Platts-Mills\
1925: S.E Baume\
1924: J.W Davidson\
1923: I.L Hjorring\
1922: P.J.G Smith\
1921: A.S Tonkin\
1920: W.E Leicester\
1919: C.G Kirk\
1918: P. Martin-Smith\
1917: Miss M. Neumann\
1916: E. Evans\
1913: Miss M.L Nicholls\
1912: O.C Mazengarb MA\
1911: F.G Hall Jones\
1910: M.H Oram MA\
1909: G.W Morice\
1908: D.S Smith\
1907: F.P Kelly\
1906: H.F O'Leary\
1905: E.J Fitzgibbon  

## Pro-Am Cup

*Presented to the winner of the bi-annual VUW Pro-Am tournament.*

2022: \[Not held]\
2021: Ellie Stevenson & and Nic Wilson\
2020: \[Not held]\
2019: Bridget Scott & Sophie Dixon\
2018: Keir Robinson\
2017: Amy Spittal\
2016: Jack Comer-Hudson\
2016: Shakked Noy\
2015: Sam O’Grady\
2014: \[Not held]\
2013: \[Not held]\
2012: Kimberley Savill\
2012: Elizabeth Begley\
2011: Aric Shakur\
2010: Asher Emanuel\
2009: Frances Ratner\
2009: Lauren Brazier\
2008: Ben Moore\
2008: Philip Belesky\
2007: Kasper Beech\
2006: Pei Huang\
2005: Mark Cordiner\
2004: Anne Molineux\
2003: Richard Frogley\
2002: Gareth Richards\
2001: Martin Smith  

## The Victoria Trophy

*Presented to the winners of the Victoria Championships.*

2023: Amy Skipper, Cara Homewood, Will Irvine\
2022: Emilie Horsfall, Emma Hardy, Eva Cochrane\
2021: Lucy Jessep, Hollie Anderson, Izzy Fraser\
2020: \[Not held]\
2019: Shine Wu, Lydia Whyte, Bridget Scott\
2018: Keir Robinson, Brooke Kinajil-Moran, Logan Rainey\
2017: Emma Westbrooke, Natalie Vaughn, Amy Spittal\
2016: Phoebe Craig, Sarah Mackenzie, Amelia Vincent\
2015: Georgia Bloor-Wilson, Harrison Fookes, Kenneth Mano\
2014: Merinda Jackson, Tamara Jenkin, Darryn Ooi\
2013: \[Not Held]\
2012: E. Begley, K. Savill, E. Munden\
2011: O. Hall, J. Gavey, D. McLachlan\
2010: N. Cross, A. Emanuel, C. Shih\
2009: R. D’Ath, P. Belesky, D. McIntyre\
2008: R. D'Ath, S. Wilson, P. Belesky, C. Howland, N. Henwood\
2007: C. Bishop, K. Errington, F. Moinfar, C. Blackford\
2006: Y. Patel, P. Huang, H. McCaffrey, C. Foulkes\
2004: F. McAlister, I. Shareef, A. Molineux, C. Dallaway\
2003: M. Mabbett, J. Clark, T. Stokell  

# International Tournaments

## Australasian Intervarsity Debating Championships (Australs)

2019: (Bali, Indonesia)\
Quarter Finalist: Victoria 1 (Sam Penno, Emily Partridge, Maddy Nash) - 8th\
Top 10 speaker: Maddy Nash (8th)  

2018: (Brisbane, Australia)\
Partial-Double Octo Finalists: Victoria 1 (Siobhan Davies, Tamara Jenkin, Oscar Battell-Wallace) - 24th  

2017: (Brisbane, Australia)\
Octo Finalists: Victoria 2 (Siobhan Davies, Taran Molloy, Oscar Battell-Wallace) - 13th  

2014: (Otago University, New Zealand)\
Winners: Victoria 1 (Asher Emanuel, Jodie O’Neill, Nick Cross) - 1st\
Top 10 speakers: Asher Emanuel (1st), Jodie O’Neill (2nd)\
Best Speaker in Grand Final: Nick Cross  

2013: (UT Mara, Malaysia)\
Finalists: Victoria 1 (Asher Emanuel, Jodie O’Neill, Nick Cross) - 2nd\
Top 10 speakers: Asher Emanuel (2nd)  

2012: (Victoria University of Wellington, New Zealand)\
Semi-finalists: Victoria 1 (Asher Emanuel, Richard D’Ath, Paul Smith) - 5th\
Octo-finalists: Victoria 2 (Nick Cross, Duncan McLachlan, Jodie O’Neill) - 12th\
Top 10 speakers: Paul Smith (8th), Richard D’Ath (10th)  

2011: (Chung-Ang University, Korea)\
Winners: Victoria 1 (Seb Templeton, Richard D’Ath, Udayan Mukherjee) – 1st\
Octo-finalists: Victoria 2 (Asher Emanuel, Paul Smith, Holly Jenkins) – 16th\
Best Speaker in Grand Final: Udayan Mukherjee\
Top 10 speakers: Seb Templeton (3rd =)  

2010: (University of Auckland, New Zealand)\
Winners: Victoria 1 (Udayan Mukherjee, Stephen Whittington, Ella Edginton) – 1st\
Semi-finalists: Victoria 2 (Seb Templeton, Richard D’Ath, Paul Smith) – 4th\
Best Speaker in Grand Final: Stephen Whittington\
Top 10 speakers: Stephen Whittington (2nd), Seb Templeton (7th=)  

2009: (Monash University, Melbourne, Australia)\
Runners-up: Victoria 1 (Polly Higbee, Stephen Whittington, Ella Edginton) – 2nd  

2008: (Ateneo de Manila University, Manila, Philippines)\
Quarterfinalists: Victoria 1 (Christopher Bishop, Polly Higbee, Stephen Whittington) – 6th\
Quarterfinalists: Victoria 2 (Kathy Scott-Dowell, Kathy Errington, Richard D'Ath) – 8th\
Top 10 Speakers: Christopher Bishop (8th), Stephen Whittington (10th)  

2007: (Universiti Teknologi Mara, Kuala Lumpur, Malaysia)\
Runners-up: Victoria 1 (Christopher Bishop, Stephen Whittington, Sayeqa Islam) – 2nd\
Octo-finalists: Victoria 2 (Polly Higbee, Clodagh O'Connor-McKenna, Josh Cameron) – 10th\
Best Speaker: Sayeqa Islam\
Best Speaker in the Grand Final: Sayeqa Islam\
Top 10 Speaker: Christopher Bishop (8th)  

2006: (Victoria University of Wellington, New Zealand)\
Quarterfinalists: Victoria 1 (Joe Connell, Stephen Whittington, Sayeqa Islam) – 5th\
Top 10 Speakers: Sayeqa Islam (4th), Joe Connell (6th)  

2005: (University of Queensland, Brisbane, Australia)\
Quarterfinalists: Victoria 1 (Christopher Bishop, Joe Connell, Gareth Richards) – 7th  

1998:\
Winners: Victoria 1 (Rob Salmond, Bryn Gandy, Matt Cuthell) – 1st\
Top 10 Speaker: Rob Salmond  

1997:\
Quarterfinalists: Victoria 1 (Hamish Finlay, Bryn Gandy, Ryan Orange)\
Top 10 Speaker: Ryan Orange  

1996:\
Quarterfinalists: Victoria 1 (Hamish Finlay, Ryan Orange, Rob Salmond)  

1982\
Winners: Victoria 1 (Jock Fanselow, Jeya Wilson, Noel Sainsbury) – 1st\
Runners-up: Victoria 2 (John Allen, Peter Biggs, Bronwen Golder) – 2nd  

1980:\
Winners: Victoria 1 (Jock Fanselow, Mary McCallum, Tim O’Brien) – 1st  

## Australasian Women and Gender Minorities' University Debating Championships (AWGMDC)

2021: (Sydney, Australia)\
Semi-Finalists: Vic 1 (Bridget Scott & Lucy Jessep)\
Semi-Finalists: Vic 2 (Izzy Fraser & Ellie Stevenson)  

2019:\
Semi-Finalist: Victoria A (Amy Spittal & Emily Partridge)  

2018:\
Runners-Up: Victoria A (Brooke Kinajil-Moran & Maddy Nash)  

2015:\
Runners-Up: Victoria A (Jodie O’Neill & Maddy Nash)  

2010: (University of Sydney Union, Sydney, Australia)\
Quarter-finalists: Victoria A (Jenna Raeburn & Ella Edginton)  

2008: (University of New South Wales, Sydney, Australia)\
Runners-Up: Victoria A (Kathy Errington & Ella Edginton)  

2007: (University of Melbourne, Australia)\
Runners-Up: Victoria A (Polly Higbee & Sayeqa Islam)\
Best Speaker: Sayeqa Islam\
Top 10 Speaker: Polly Higbee (3rd)

## World University Debating Championships

2021: (Belgrade, Serbia)\
Octo Finallists: Victoria A (Sam Penno, Keir Robinson)  

2020: (Bangkok, Thailand)\
Octo Finalists: Victoria A (Shakked Noy, Taran Molloy) - 34th\
Top 100 Speakers: Shakked Noy (65th), Taran Molloy (81st)  

2019: (Capetown, South Africa)\
Top 100 Speakers: Maddy Nash (79th)  

2018: (Mexico City, Mexico)\
Octo Finalists: Victoria A (Shakked Noy, Maddy Nash) - 44th\
Top 100 Speakers: Maddy Nash (52nd), Shakked Noy (55th)  

2017: (The Hague, The Netherlands)\
Breaking Judges: Jodie O’Neill (grand final), Seb Templeton (EFL Final), Udayan Mukherjee (EFL Final), Olivia Hall (grand final), Philip Belesky (Octo-Final)  

2016: (Thessaloniki, Greece)\
Breaking Judges: Jodie O’Neill, Udayan Mukherjee (grand final), Sebastian Templeton (grand final), Chuan-Zheng Lee, Stephen Whittington  

2015: (Malaysia)\
Quarter Finalists: Victoria A (Kimberley Savill, Daniel Wilson)- 12th\
Top 100 speakers: Daniel Wilson (46th), Kimberley Savill (54th)\
Breaking Judges: Seb Templeton (semi final, ESL grand final), Chuan-Zheng Lee (quarter final)  

2014: (Chennai, India)\
Octo Finalists: Victoria A (Asher Emanuel & Jodie O’Neill) - 25th\
Top 100 speakers: Asher Emanuel (17th), Jodie O’Neill (29th), Olivia Hall (84th),\
Breaking Judges: Udayan Mukherjee, Richard D’ath, Stephen Whittington  

2013: (Berlin Debating Union, Berlin, Germany)\
Octo-finalists: Victoria A (Seb Templeton & Richard D’Ath)- 16th\
Top 100 speakers: Seb Templeton (10th), Richard D’Ath (12th)\
Breaking judges: Philip Belesky (semi-finals)  

2012: (Ateneo de Manila University, Philippines)\
Quarter-finalists: Victoria A (Udayan Mukherjee & Stephen Whittington)- 7th\
Octo-finalists: Victoria B (Seb Templeton & Richard D’Ath) - 8th\
Top 100 speakers: Stephen Whittington (3rd), Udayan Mukherjee (8th), Seb Templeton (15th), Richard D’Ath (15th), Asher Emanuel (82nd)\
Breaking judges: Philip Belesky (semi-finals)  

2011: (University of Botswana, Gaborone, Botswana)\
Top 100 speakers: Udayan Mukherjee (41st)  

2010: (Koc University, Turkey)\
Octo-finalists: Victoria A (Polly Higbee & Stephen Whittington)- 29th\
Top 100 speakers: Seb Templeton (18th), Polly Higbee (37th), Stephen Whittington (41st), Ella Edginton (41st)\
Breaking judges: Christopher Bishop (Grand Final), Sayeqa Islam (ESL Grand Final, Main semi-finals), Jonathan Orpin (main semi-finals), Jesse Wilson (ESL semi-finals), Gareth Richards.  

2009: (University College Cork, Cork, Ireland)\
Semi-finalists: Victoria A (Polly Higbee & Stephen Whittington) - 23rd\
Top 100 speakers: Stephen Whittington (15th), Polly Higbee (25th)\
Breaking judges: Christopher Bishop (semi-finals)  

2008: (Assumption University, Bangkok, Thailand)\
Octofinalists: Victoria A (Christopher Bishop & Gareth Richards) - 20th\
Octofinalists: Victoria E (Joe Connell & Kathryn Scott-Dowell) – 27th\
Top 100 speakers: Christopher Bishop (29th), Josh Cameron (51st), Joe Connell (55th),\
Gareth Richards (59th), Sayeqa Islam (59th), Michael Mabbett (74th),\
Clodagh O'Connor-McKenna (84th), Stephen Whittington (84th), Polly\
Higbee (100th)  

2005: (Multimedia University, Kuala Lumpur, Malaysia)\
Octofinalists: Victoria B (Christopher Bishop & Gareth Richards) – 25th\
Octofinalists: Victoria A (Joe Connell & Sayeqa Islam) – 31st\
Top 100 speakers: Christopher Bishop (42nd), Gareth Richards (49th), Sayeqa Islam (75th),\
Joe Connell (84th)\
Breaking judges: Kevin Moar (quarter-finals)  

2004: (Nanyang Technological University, Singapore)\
Top 100 speaker: Chelsea Payne (100)\
Breaking judges: Kevin Moar (quarter-finals)  

2003: (Stellenbosch University, South Africa)\
Top 100 speakers: Chelsea Payne (71st), Nicola Willis (83rd)  

2002: (Hart House, University of Toronto, Toronto, Canada)\
Top 100 speaker: Nicola Willis (42nd)  

2000: (University of Sydney, Sydney, Australia)\
Semifinalists: Victoria A (Matt Cuthell & Angela Ballantyne) - 6th\
Top 100 speakers: Matt Cuthell (15th), Angela Ballantyne (29th)  

1999:\
Octofinalists: Victoria A (Matt Cuthell & Tammy Schultz) (18th)\
Top 100 speakers: Matt Cuthell (72nd), Tammy Schultz (83rd)  

1998:\
Octofinalists: Victoria B (Rob Salmond & Ryan Orange) (13th)\
Top 100 speakers: Rob Salmond (16th), Ryan Orange (24th)  

1997:\
Octofinalists: Victoria A (Hamish Finlay & Bryn Gandy) (19th)  

1994:\
Quarterfinalists: Victoria A (? and ?)  

1988:\
Grand Finalists: Victoria A (Vanessa Homewood and Raybon Kan) (broke 9th)\
Top 100 speakers: Vanessa Homewood (54th), Raybon Kan (83rd),  

1986:\
Top 100 Speakers: Iain Rennie (20th), Noel Sainsbury (21st), Andrew Stockley (22nd)  

1981:\
Breaking team: Victoria (16th)?\
Top 100 Speakers: Gerard Winter (2nd)  

## UK and Ireland Debating Tournaments

Cambridge Union IV 2007:
Winners: Victoria (Christopher Bishop & Sayeqa Islam)
Top 10 speakers: Sayeqa Islam (2nd); Christopher Bishop (4th)

Oxford Union IV 2007:
Runners-up Victoria (Christopher Bishop & Sayeqa Islam)
Top 10 speakers: Christopher Bishop (7th)

University College Dublin Vice-President’s Cup 2007:
Runners-up Victoria (Christopher Bishop & Sayeqa Islam)
Top 10 speakers: Christopher Bishop (3rd); Sayeqa Islam (6th)

# Awards

## Victoria Debating Sporting Blues

### I. Sporting Blue

*The Victoria University of Wellington Students' Association awards sports blues each year for outstanding sporting achievements. Blues are the highest recognition the university can give to people who have, by their sporting achievements, brought credit to the institution.*

Bridget Scott — 2021\
Sam Penno - 2021\
Keir Robinson — 2021\
Ellie Stevenson — 2021\
Oscar Battell-Wallace — 2018\
Christopher Bishop — 2004, 2005, 2007, 2008\
Josh Cameron — 2007\
Joe Connell — 2005, 2006\
Nick Cross — 2014\
Richard D'Ath — 2008, 2009, 2010, 2011\
Hamish Finlay — 1996, 1997?\
Ella Edginton — 2009, 2010\
Asher Emanuel — 2011, 2014\
Kathy Errington — 2008\
Bryn Gandy — 1996, 1997\
Polly Higbee — 2006, 2007, 2008, 2009, 2010\
Lewis Holden — 2006\
Sayeqa Islam — 2003, 2006, 2007\
Tamara Jenkin — 2018\
Holly Jenkins — 2011\
Udayan Mukherjee — 2010, 2011\
Maddy Nash — 2016, 2018\
Shakked Noy — 2018\
Jodie O’Neill — 2014\
Ryan Orange — 1996, 1997?\
Gareth Richards — 2004, 2005, 2008\
Kathy Scott-Dowell — 2008\
Rob Salmond — 1996, 1997, 1998\
Paul Smith — 2010, 2011\
Seb Templeton — 2009, 2010, 2011\
Stephen Whittington — 2006, 2007, 2008, 2009, 2010\
Daniel Wilson — 2015  

### II. Sportsperson of the Year

Sayeqa Islam — 2007\
Stephen Whittington — 2010  

## Blues Awards for Administration

### I. Victoria Administration Blues

*The Victoria University of Wellington Students' Association awards blues each year for outstanding sports administration.*

Oscar Battell-Wallace — 2015, 2016, 2017\
Christopher Bishop — 2006, 2008\
Eleanor Bishop — 2006\
Polly Higbee — 2006\
Tamara Jenkin — 2016\
Ruby Meagher — 2016\
Udayan Mukherjee — 2011\
Maddy Nash — 2016, 2017\
Shakked Noy — 2017\
Kimberley Savill — 2015\
Jodie O’Neill — 2014\
Torie Olds — 2006\
Ryan Orange — ?\
Gareth Richards — 2006\
Seb Templeton — 2010, 2011\
Daniel Wilson — 2011, 2014\
Sarah Wilson — 2010  

### II. Sports Administrator of the Year

Johanna McDavitt — 2012\
Seb Templeton — 2010, 2011\
Christopher Bishop — 2006, 2008\
Jo Benseman  

## New Zealand Blues

*The NZU Blue is one of New Zealand’s most prestigious and long standing awards that recognises a University student’s performance in representing New Zealand at the annual pinnacle event of their sport whilst successfully progressing their academic studies.*

Andrew Stockley — 1987\
Alexandra Gillespie — 1994\
Bryn Gandy — 1998\
Ryan Orange — 1997\
Robert Salmond — 1997, 1998\
Anna Livesey — 2000\
Chelsea Payne — 2000, 2002\
Duncan Small — 2002\
Sayeqa Islam — 2003, 2006, 2007\
Gareth Richards — 2004\
Christopher Bishop — 2004, 2005, 2007\
Joseph Connell — 2006\
Stephen Whittington — 2006, 2007, 2008, 2009, 2010\
Josh Cameron — 2007\
Polly Higbee — 2007, 2008, 2009\
Ella Edginton — 2009, 2010\
Seb Templeton — 2010  

## Gold Awards

*The gold awards are presented by VUWSA to recognise clubs and student who contribute to the culture of Vic.*

### Cultural Gold Awards

Lucy Jessep — 2023\
Peter Lang — 2023  

### Contributions to Clubs

Lucy Jessep — 2023  

# Significant Figures

## Life Members

\[Not accurate records. Needs to be revised.]\
Jodie O’Neill — 2015\
Udayan Mukherjee — 2013\
Richard D’Ath — 2013\
Sebastian Templeton — 2012\
Polly Higbee — 2010\
Steven Whittington — 2010\
Christopher Bishop — 2009
Gareth Richards — 2008\
Sayeqa Islam — 2008\
Kevin Moar — 2006\
\[Records don’t go back further than this, Others we know of]\
Neil Miller — 1996\
Ryan Orange — 1997\
Alexandra Gillespie — 1993\
Philippa Doyle — 1990\
Vanessa Homewood — 1990\
Don Stevens — 1986\
Brendan McPadden O’Connor — 1974\
Catherine S. Forde — 1972  

## Past Execs

2024:\
President: Emma Hardy\
VP: Rebecca Elder\
Secretary: Ollie Gillespie\
Equity: Cara Homewood\
Treasurer: Peter Gedye\
Fresher Rep: Xandi Cooke\
General Exec: Lucy Jessep, Anton Weitzel, Peter Lang, Sophie Land, Sophie Crozier/Jack Turnbull  

2023:\
President Lucy Jessep\
VP: Hollie Anderson\
Secretary: Emilie Horsfall\
Equity: Rebecca Elder\
Treasurer: Aidan Homewood\
Fresher Rep: Sophie Crozier\
General Exec: North, Emma Hardy, Gabe Melrose, Anton Weitzel, Peter Lang  

2022:\
President: Bridget Scott\
VP: Florence Oakley\
Secretary: Samuel Baker\
Equity: Lucy Jessep\
Treasurer: Adam Muir\
Fresher Rep: Emilie Horsfall\
General: Hollie Anderson, Hattie Compton-Moen/North, Peter Lang, Izzy Fraser, Connor Molloy  

2021:\
President: Sam Penno\
VP: Bridget Scott\
Secretary: Meg Longley\
Equity: Brooke Kinajil-Moran\
Treasurer: Frankie Gross\
Fresher Rep: Jamie Yee\
General: Ellie Stevenson, Connor Molloy, Samuel Baker, Florence Oakley, Keir Robinson  

2020:\
President: Brooke Kinajil-Moran\
VP: Keir Robinson\
Secretary: Bridget Scott\
Equity: Sophie Dixon\
Treasurer: Frankie Gross\
Fresher Rep:\
General: Samuel Penno, Sarah Mackenzie, Lily Stelling, Toby Hunter, Emily Partridge  

2019:\
President: Amy Spittal\
VP: Taran Molloy\
Secretary: Sam Penno\
Equity: Brooke Kinajil-Moran\
Treasurer: Jack Boltar\
Fresher Rep: Bridget Scott\
General: Keir Robinson, John Paver, Alvien Benitez, Alister Hughes, Toby Major  

2018:\
President: Sam O’Grady\
VP: Amy Spittal\
Secretary: Shakked Noy\
Equity: Ailidh Leslie\
Treasurer: Jack Boltar\
Fresher Rep: Max Salmon\
General: Maddy Nash, Taran Molloy, John Paver, Rob Teagle, Kathleen Best  

2017:\
President: Sam O’Grady\
VP: Emily Scrimgeour\
Secretary: Siobhan Davies\
Equity: Ailidh Leslie\
Treasurer: Jack Boltar\
Fresher Rep: Nat Vaughan\
General: Shakked Noy, John Paver, Kathleen Williams, Taran Molloy, Maddy Nash  

2016:\
President: Maddy Nash  

2015:\
President: Kimberley Savill  

2014:\
President: Jodie O’Neill  

2013:\
President: Jodie O’Neill  

2012:\
President: Udayan Mukherjee  

2011:\
President: Seb Templeton  

2010:\
President: Seb Templeton  

2009:\
President: Polly Higbee  

2008:\
President: Chris Bishop  

2007:\
President: Chris Bishop  

2006:\
President: James Clark  

2005:\
President: Gareth Richards  

2004:\
President: Sayeqa Islam  

2003:\
President: Malcolm Birdling  

2002:\
President: Chelsea Payne  

2001:\
President: Chelsea Payne  

2000:\
President: Antonia Reid  

1999:\
President: Matthew Cuthell  

1998:\
1997:\
1996:\
1995:\
1994:\
1993:\
1992:\
1991:\
1990:\
President: Vanessa Homewood  

1989:\
1988:\
1987:\
1986:\
1985:\
1984:\
1983:\
1982:\
1981:\
1980:\
1979:\
1978:\
President: Chris Chapman  

1977:\
President: Chris Chapman  

1976:\
President: Linda Hardy  

1975:\
President: Crawford Falconer\
1974:  

1973:\
President: John Blincoe  

1972:\
President: John Blincoe  

1971:\
President: AA Curran  

1970:\
1969:\
1968:\
1967:\
1966:\
President: A H Ashenden  

1965:\
1964:\
President: R Chapman  

\[At some point Chairmain used to be the equivalent to president, and the president was the lecturer in charge]  

## Past Patrons

Governor General — 1972 - 1976\
Chief Justice of New Zealand — 1947\
Governor-General — 1945 - 1946\
Governor-General, Marshal of the Royal Air Force Sir Cyril Newall — 1941 - 1944\
Governor-General — 1937\
Governor-General, Lord Bledisloe — 1931\
Governor-General, Viscount Jellicoe — 1921  

# Club Awards

## Gold Awards

2022 — Sports Club of the Year\
2023 — Supreme Club of the Year  

{{< extraCSS >}}
  #content {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
  #content .hash-link h1 {
    text-align: center;
  }
  h2, h3, p {
    width: min(550px, 90vw);
  }
  details {
    align-self: stretch;
  }
{{< /extraCSS >}}
