---
layout: resource
title: Impacting
cover_image: /uploads/iu2.jpeg
draft: false
publishDate: 2023-11-09
summary: If a tree falls in the woods, does the adjudicator care?
---
Once you have explained why an outcome is likely to happen, you need to explain why the adjudicator should care. This is called impacting, or weighing. It is best to combine multiple types to make the point more important, and thus harder to mitigate out.

There are 7 ways to weigh:

* **Scale of impact** - If it affects more people it is more important
* **Significance of impact** - If it makes more of a difference (positive or negative) it is more important
* **Certainty** - If it is guaranteed to occur it is more important than something that may not occur
* **Vulnerability** - If the people it affects are particularly in need of help or would be unable to help themselves, it is more important than helping people who are doing fine
* **Timeframe** - If it happens sooner it is more important, because otherwise we will not be in a position to solve long term issues.
* **Principle duty** - If there is some principle we as a society value, we should try and act according to those principles. This includes righting historic wrongs e.g. honouring Te Tiriti claims. Sometimes it may be important to explain why we value these principles.
* **Quality of argument** - If an argument is clearer, more robustly explained, and generally more intuitive it is more likely that it is true. Remember to explain *why* your argument is explained better. Perhaps it relies on fewer links, or the mechanisms are obviously true.

Often the adjudicator will not completely be convinced that a team has proven or disproven points. When this happens, they will have to weigh up different points in order to determine which is the most important, and thus who should win. This is why it is important to impact your points heavily (explain why they are important), mitigate your opponents points (explain why they are not important), and weigh them up (compare them so the adjudicator doesn't have to).
