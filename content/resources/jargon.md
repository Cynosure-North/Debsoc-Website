---
layout: resource
title: Jargon
cover_image: /uploads/teacher.jpg
draft: false
publishDate: 2023-11-09
summary: ‘An extensive specialized vocabulary exists to describe these
  relatively ordinary events.’ - Sally Rooney
---
{{< contents >}}

# Basic Terms

## Aff & Neg

The two teams in australs and impromptu style. Aff (short for affirming) support the motions, and Neg (short for negating) oppose the motion.

## Government & Oppostition

The equivalent of aff and neg in a BP debate.

## Moot

The topic of the debate. They are written as THX, TH is short for This House a.k.a. the affirming team. There are a few types of moot:

> *THW* - This house would X
>
> > Aff must explain why doing X would be a good thing
>
> *THB / TBHT* - This house believes X /This house believes that X
>
> > Aff must explain why X is true.
>
> *THS* - This house supports X
>
> > Aff must explain why X is good for the world.
>
> *THO* - This house opposes
>
> > Like THS, but Aff must explain why X is bad.
>
> *THP* - This house prefers X to Y
>
> > Like THS, but Neg must argue in support of Y.
>
> *THR* - This house regrets X
>
> > Aff must first explain what the world would look like if X never existed, and then explain why that's desireable. THR moots can trip people up.
>
> *TH, as Z* - This house as Z would X
>
> > Aff must explain what Z cares about, and therefore why they would do/support X.

## Adj / Adjudicator

Another name for judges.

## Chair

The most senior adjudicator in a debate, who typically has the most influence over the response.

## OA / Oral Adjudication

The explanation from the adj at the end of the round, covering who won, why, and usually giving feedback.

# Tournament Details

## Accreditation

Adjs in NZ must be certified. This is to ensure that adjs can correctly assess debates, and give clear OAs and helpful feedback. Once someone passes this they're said to be accredited.

## Trainees

People who are learning to adj. In order to accredit they have to trainee first. This involves practicing as an adj beside people who have already accredited. To learn more go to our [Trainee FAQ](/resources/trainee-FAQ).

## Adj Core

The team of chief adjudicators (CAs) who are repsonsible  for setting motions and allocating adjs.

## Supers

Adjs who have been the CA for a major. Two supers from different campuses must sign off for a trainee to accredit.

## Majors

The biggest tournaments, and the highlight of the debating calendar. New Zealand's majors are Joynt Scroll and Officers Cup. The International Majors Vic attends are Australs and Worlds

## Minors / Minis

All the other intervarsity tournaments that aren't majors. New Zealand's minors are NZWGM, Thropy, Claytons, NZBP, Auckland IV, and occasionally other tournaments.

## The Tab

The record of every result at a tournament. After adjs complete a ballot they submit it to Adj core who use it to decide matchups for the next round.

## In & Out Rounds

In rounds are open to everyone. After a number of in rounds the break is announced. After this the tournament moves into the out rounds, usually semi-finals and finals in which only the top teams speak. In rounds are power paired, out rounds are single elimination.

## Breaking

Making it through to the out rounds. The break is the group of teams who make it through, and the breaking judges are the ones who judge them

## Ameteurs / Ams / Novices

Fairly self explanatory, people who are new. Some tournaments limit team structure, such as as pro-ams, or am-only tournaments like Thropy. Check the [tournament guide](/resources/tournaments-we-attend/) for specifics.

## Pros

Someone who is not an am.

TODO: We can probably remove this, it's obvious

TODO: Just like this terrible joke lampshading it

## Pro-am

A tournament in which teams much be made up of a mixture of pros and ams. Usually 1 pro and 2 ams in a team of 3.

## Silent Round

A round with no OA. The final in round is usually a silent round. Many adjs are happy to reveal the outcome of the debate after the break is announced

## Tramming

When the timetable of a tournament is adjusted to compensate for running late. This is extremely common. The name comes from a tournament in Sydney which ran late because everyone got stuck waiting on trams.

## Splitting

When one or more judges disagree with each other. Typically the minority is said to have split from the panel. Judges try to avoid this because theoretically they should all come to the same, objective, decision. Not to be confused with a split point.

## WGM

Stands for Women and Gender Minorities. This means women, enbies, and and transmasc people. It's intentionally weighted towards inclusion, but if you're unsure you're always welcome to ask if that includes you.

# Parts of a Speech

## Substantive

New arguments and analysis that create a case for or against a motion. These are the constive points a first speaker will run.

## Rebuttal

The  counter-argument to a team's substantive, explaining why it's not true or not relevant.

## Burden / Path to Victory

What a team needs to prove in order to win the debate. Teams will often set their own PTVs to guide the adjudicator's thinking

## Model

The plan proposed by a team to get an outcome. This could include the laws that will be changed, where funding will be allocated, or anything else that will change how the system works. Typically presented by the affirmative, but if they don't present one neg can.

## Counter-Model

A model proposed by a team on the opposition.

## Characterisation

The motivations and capabilities of any parties involved in the debate. For example, corporations want to make money and they can develop and sell goods in order to achieve that.

## Context

The background knowledge the judge needs to understand for a point to make sense. These are usually uncontroversial facts about the world. For example, the sky is blue, NZ is an island nation.

## Mechanism

The explanation of the steps between the context and the outcome. I.e. the explanation of why something is true. For example, the apple fell because of gravity.

## Assertion

Claiming something is true without providing sufficient mechanisms. Generally this is bad, but if it's something uncontroversial then it's fine.

## Weighing

The explanation of the why the outcomes of the mechanisms are important in the world of the debate, and how each clash ranks compared to each other clash.

## Meta/Commentary

The explanation of the impact of a point on other points within the debate. This could be highlighting that a line of rebuttal undermines the key point of the oppositions case, or that a particular mechanism explains a missing link in a previous speech.

## Actor

A person or organisation who is significant in the debate. It is important to characterise them. You don't have to pretend to be the person, but it's always encouraged (it doesn't help you win though).

## Split point

A substantive point run at second speaker. Sometimes just referred to as a split. Typically, and ideally, it presents an independant path to victory. Not to be confused with splitting.

# Rarer terms

## Squirrel

An unreasonable attempt by the affirmative team to restrict a motion, or change it’s definition from the ‘spirit’ or intended meaning of the motion.

## Ironmanning

When a team speaks with fewer members than is required. This means each speaker has to do multiple speeches. Typically teams are only allowed one speaker missing, and only for a limited number of debates. Usually one speaker will do first and third, and the other will do second and reply. It is called supermanning if one speaker does all speeches.

## Knifing

When a speaker contradicts their team. This is considered extremely poor form and will result in losing points in the strategy section. It's important to be careful of this in BP because closing half must accept what their opening half has said. Sometimes it's possible to explain why everything that has already been said actually agrees with this new material, but if it gets to that point something has gone wrong.

## Strawmanning

When a speaker misreprents the case of the opposition in order to attack it. Adjs can see through this and will deduct points. It also leads to unintersting debates.

## Delta

The difference between each team's version of the world. Using this world is considered excessively jargony on the NZ circuit.

## Symmetric

When a point is the same on either side, i.e. there is no delta.

## Revealed Preference

The hidden interests of an actor which are revealed through their past actions. Using this world is considered excessively jargony on the NZ circuit.

# Advanced Concepts

## Fiat

The power to just say that's how things are. It can be a complex topic to understand how much the moot gives, and it can trip up even experienced teams.

In any given moot some change must be possible for the debate to happen. For example, if the moot was THW ban zoos, the debate couldn't proceed if zoos can't be banned. Because of this Aff is allowed to assert there is the political will to do so. Neg is also allowed to propose a countermodel with a similar viability.

If Aff proposed making zoos illegal, Neg could instead propose regulating them more strongly, but it would be unreasonable to propose developing technology to let the animals speak human languages and therefore advocate for themselves.

## Steelmanning

When a speaker takes the opposition at their best and still proves why they're wrong. This means accepting all their wild assertions, undermeched points, and moon logic, and explaining why all of that doesn't matter. This is the opposite of strawmanning.

# Administration

## NZSDC

New Zealand Schools' Debating Council is an organisation that organises Schools' Nationals, coaching for the teams, regional development teams, the NZ Schools' Worlds team. Find more on [their website](https://www.debating.org.nz/)

## NZUDC

New Zealand Universities' Debating council is a minor organisation that manages the NZ Uni debating circuit, and coordinates tournaments.

# Jokes

## The Essay

Salley Rooney is a former debater who wrote an essay about debating, and got a book deal from it. People in the circuit have opinions on it. You can read it here: [Even if you beat me](https://thedublinreview.com/article/even-if-you-beat-me/)
