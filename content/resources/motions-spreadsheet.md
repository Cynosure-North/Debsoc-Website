---
layout: redirect
title: Motion Spreadsheet
cover_image: /uploads/records.jpg
publishDate: 2024-02-28
summary: The motions set in NZ, going back over 100 years. Also check out hellomotions.com
redirectURL: https://docs.google.com/spreadsheets/d/1cTf6ZYCI_i-nfFXP_sX7i0uNZZqBoUbNoNzzdQeV5O4/edit?usp=sharing
---