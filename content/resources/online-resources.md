---
layout: resource
title: Online Resources
cover_image: /uploads/piles-of-french-novels.jpg
draft: false
publishDate: 2024-04-08
summary: Useful links if you want to train on your own (Talk to exec
  though, they can help facilitate group dev sessions).
---
[Global Tournament Spreadsheet](https://docs.google.com/spreadsheets/u/0/d/1R9s3MAh1H_7rJ9NQhO18p6o7bvekrIDTk27l7emXk6o/htmlview?pli=1#gid=261973339)

——

[Open BP Debate Resources](https://docs.google.com/spreadsheets/d/1crrb6ru8wu7S_aaY_EgZ7OKhoYs-wI145OpRJ--MyZ4/edit#gid=1772318052)

[Debating's online universe](https://docs.google.com/spreadsheets/d/1Zr4DiGRh7i6IWlQHWnwtNrSUigUnR43kVT5oaNl0hII/edit#gid=1989072403)

[How to Debate [Blog]](https://howtodebate.blogspot.com/)

[Eh Priori [Blog]](https://ehpriori.com/)

[Trolley Problem [Blog]](https://trolleyproblem.blogspot.com/)

[New Zealand University Debating [Facebook group]](https://www.facebook.com/groups/1523148774563845)

[Debating Seriousposting [Facebook group]](https://www.facebook.com/groups/communistcasefile/)

[Debating Shitposting [Facebook group]](https://www.facebook.com/groups/1369733616370998/)

[Australian Debating [Facebook group]](https://www.facebook.com/groups/292752334212789/)

[Asia Debating [Facebook group]](https://www.facebook.com/groups/asiainberlin/)
