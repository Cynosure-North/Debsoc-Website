---
layout: resource
title: Our Constitution
cover_image: /uploads/waikato-manukau_sheet-_the_treaty_of_waitangi.jpg
publishDate: 2023-11-09
summary: The rules that govern debsoc. You probably don’t need to read this.
---

{{< summary "Preamble" >}}
The Constitution of the Victoria University of Wellington Debating Society, Te Roopu Taukumekume o te Whare Wanoanga o te Upoko o te Ika a Maui Incorporated  

[Common Seal]  

est. 1899  



Adopted  
This 28 day of July 2005  




Signed                                     Signed  

Gareth Richards                    Joseph Connell  
President                                 Vice-President  


For and on behalf of the Victoria University of Wellington Debating Society, Te Roopu Taukumekume o te Whare Wananga o te Upoko o te Ika a Maui Incorporated.  


[common seal]  

{{< /summary >}}
{{< contents >}}

1. # Name
   1. ## The name of the society is:  
   THE VICTORIA UNIVERSITY OF WELLINGTON DEBATING SOCIETY, TE PAEMANU O TE HERENGA WAKA INCORPORATED, hereinafter referred to as “the Society”.
1. # Objects
   1. ## The objects of the society are:
      1. To take over the effects and liabilities of the existing unincorporated society known as the Victoria University Debating Society.
      1. To foster and promote debating and public speaking among students at Victoria University and to further the interests of the society in Wellington and elsewhere.
      1. To arrange, conduct and assist in the arranging and conducting of debating and public speaking contests and events.
      1. To do all such things as are incidental or conducive to the attainment of the above objects and for this purpose to have all or any of the powers set forth in the following Article 3.
1. # Powers
   1. ## The Society shall possess the following powers:
      1. To conduct, control, and administer the Plunket Medal Oratory contest, the Union prize competition, and any other debating or public speaking contest or competition, which may be established, and the control of which is vested in the Society.
      1. To co-operate or affiliate with any other organisation having objects altogether or in part similar to those of the Society.
      1. To participate in any debating or public speaking contest or event, and to select representative individuals or teams for this purpose.
      1. To purchase, lease, hire, or by any other means acquire any property or rights.
      1. To sell, hire, exchange, manage, improve, develop, or otherwise deal will all, or any part of, the property or rights of the Society, or in which the Society has, or may hereinafter have, any beneficial interest, and to raise money on the security of the same.
      1. To invest such of the Society’s funds as are not required for the immediate use of the Society.
      1. To borrow, or raise money in such a manner as shall be deemed expedient, which may include the issue of debentures, bonds, or mortgages, bank overdraft, or any other security, which may be founded or charged if need be, upon the floating assets or upon all or any of the property and rights of the Society, present or future.
      1. To guarantee the payment of money or the performance of obligations by or to otherwise assist any person or organisation, where those obligations relate to the Society’s objects and which will lead to no pecuniary profit for any member.
      1. To appoint trustees to all or any of the funds or property of the Society, and to vest such funds or property in such trustees.
      1. To engage any such persons whose services may be deemed necessary or convenient for the purposes of the society’s objects, provided always that:
         1. No member of the Society or any person associated with a member shall participate in or materially influence any decision made by the Society in respect of the payment to or on behalf of that member or associated person of any income, benefit, or advantage whatsoever.
         1. Any such income paid shall be reasonable and relative to that which would be paid in an arm’s length transaction (being the open market value). The provisions and effect of this clause shall not be removed from this document, and shall be included and implied into any document replacing this document.
1. # Membership
   1. ## Ordinary Membership
      1. Application for membership shall be made in writing and in such form as the Committee may from time to time specify and shall be made to the Secretary.
      1. Any person shall be eligible for ordinary membership of the society who:
         1. Is a person falling within one of the persons or classes of persons referred to in s3(2) of the Victoria University of Wellington Act 1961; AND
         1. Is enrolled as a student at the Victoria University of Wellington (“the University”) and at the time of application is in the opinion of the Committee, the composition of which is detailed in article 7, then attending the University as a graduate or undergraduate student. The Committee may accept as sufficient compliance with this requirement any declaration by a student that they are enrolled as a student and that either:
            1. they are currently attending the University as a student; OR
            1. they have at any time in the immediately preceding 6 months attended the University as a student; OR
            1. they intend to study at the University as a student within the next 3 months.
   1. ## Non-Student Membership
      1. Application for non-student membership shall be made to the Secretary.
      1. Any person shall be eligible for non-student membership of the Society. However their membership may be declined by a majority motion of the Committee within two weeks of their signing up.
      1. Any non-student member may resign from the Society by notification to the Secretary of the Society.
      1. Non-student members shall be entitled to attendance and speaking rights at General Meetings of the Society. Non-student members possess voting rights within the Society, but shall be barred from holding any office excepting that of Patron.
   1. ## Payment of Membership
      1. All ordinary and non-student members must pay the annual subscription fee in order to be a member of the society.
      1. The annual subscription fee for membership is determined by the committee.
   1. ## Termination of Membership
      1. Failure to pay
         1. Any ordinary or non-student member whose subscription is not paid within two months of the Initial General Meeting shall be disentitled to the privileges of membership and their name shall be removed from the Society’s register of members, unless and until such subscription is paid.
      1. Resignation of membership
         1. Any member may resign from their membership by giving to the Secretary notice in writing to that effect and every notice unless otherwise expressed and agreed to by the Society shall take effect at the expiration of one calendar month after the giving of such notice but any such resignation shall not release the member from payment of their subscription to the day of their resignation.
      1. The Committee’s ability to terminate ordinary or non-student membership
         1. Should any member be deemed by the Committee to have conducted themselves in a grossly inappropriate manner likely to prejudice the ability of other members to participate fully in the society, such member will be officially warned in writing by a majority motion of the Committee. In the event that the member concerned is a Committee member, they shall not be entitled to vote in this regard. Such behaviour includes, but is not limited to, sexual harassment of another member, assault of another member and fraud against the society.
         1. Should the behaviour persist, the Committee has the power to terminate the membership of the offending person, provided that the Committee's vote on termination is unanimous. In the event that the member concerned is a Committee member they shall not be entitled to vote in this regard.
         1. Persons whose membership has been terminated have a right of appeal to the Victoria University of Wellington Students’ Association, and then to the Victoria University Council.
         1. Persons who have had their membership terminated may not rejoin the Society in subsequent years without the unanimous approval of the Committee or by a two-thirds majority at a General Meeting.
1. # Life Membership
   1. ## General
      1. Any person may be elected a life member of the Society, in recognition of the outstanding excellence in debating and public speaking, or in recognition of outstanding services rendered to the Society.
      1. The threshold for Life Membership is incredibly high and is intended to be given to the most outstanding members of the society.
      1. Life Members shall be entitled to attendance and speaking rights at General Meetings of the Society. Life Members shall possess the same voting rights as all other members, except that they may vote if they have not paid membership fees.
      1. Life Membership shall be conferred at the Annual General Meeting of the Society in the manner hereinafter prescribed.
      1. Candidates for Life Membership shall be nominated by two members of the Society at the Annual General Meeting.
      1. Election to Life Membership shall take place by secret ballot at the Annual General Meeting, provided that at least three-fourths of those present and entitled to vote, vote in favour of the election of the nominee.
      1. Life Membership may also be passed by acclamation at an Annual General Meeting, provided that no-one opposes the candidate being elected, namely the acclamation must be unanimous, and furthermore provided that no-one objects to electing the candidate by acclamation. The chairperson must provide ample opportunity for people to object. If anyone present and entitled to vote opposes the election or asks for a secret ballot regarding the election, then a secret ballot must be undertaken in the same manner as is done to elect members of the committee.
1. # The Committee
   1. ## Powers of the Committee
      1. The Committee shall manage the affairs of the Society.
      1. The Society shall be bound by any decision properly made by the Committee at a meeting duly convened, provided that the Committee shall be bound by, and shall have no power to override or set aside any decision properly make by a General Meeting of the Society.
      1. The Committee may exercise and perform all such powers and functions of the Society which are not otherwise delegated by this constitution.
   1. ## Composition of the Committee
      1. The Committee shall be composed of:
         1. The Officers of the Society, namely the President, the Vice-President, Secretary, Treasurer and Equity Officer;
         1. five other committee representatives; and
         1. a first year representative (to be elected at the Society’s Initial General Meeting) or such number of other members as the Annual General Meeting may determine.
      1. All members of the Committee must be members of the Society at the time of their election and throughout their term of office.
      1. All members of the Committee must be an enrolled student at Victoria University of Wellington at the time of their election.
   1. ## Vacancies upon the Committee
      1. If any officer of the Society shall die, resign, or absent themselves without leave of absence, a special general meeting of the Society shall be summoned forthwith to elect a successor, who shall hold office until the end of the ensuing Annual General Meeting.
      1. If any other member of the Committee shall die, resign, or absent themselves from three consecutive meetings of the Committee without leave of absence, the Committee may elect a successor to hold office until the end of the ensuing Annual General Meeting.
      1. It shall be the duty of members of the Committee to attend all meetings of the Committee, but if for reason deemed good and sufficient by the Committee a member cannot attend any such meeting, they shall be granted leave of absence.
   1. ## Meetings of the Committee
      1. All provisions under article 9 that apply to General Meetings shall insofar as they are not inconsistent with provisions in article 6 and 7 apply to committee meetings as well.
      1. The Committee shall meet at frequent intervals throughout the year to conduct the affairs of the Society
      1. Meetings of the Committee may be called by the President or by the Secretary at any time, or on such dates as the Committee shall determine.
      1. The Secretary must summon a meeting of the Committee forthwith upon the requisition of any two Committee members, such requisition to be addressed to the Secretary, and to set out specifically the business for which the meeting of the Committee is required
      1. The Secretary or President shall provide notice to all members of the Committee, of the date, time, and venue, of a meeting of the Committee, and of the business to be considered thereat.
      1. A meeting of the Committee shall only be valid if it is properly summoned, and proper notice is given.
      1. The Secretary shall determine the order of business at all meetings of the Committee, provided that the Committee may at any stage amend the order of business of the Committee; there shall be an opportunity for the raising of general business.
      1. No business shall be transacted at a meeting of the Committee unless a quorum of members is present. A majority of the members of the Committee shall constitute a quorum.
      1. Votes by proxy shall not be permitted.
   1. ## Non-Committee members and Committee Meetings
      1. All members of the society shall have speaking rights at a committee meeting.
      1. However, if the Committee believes the discussion of a non-Committee member to be prohibiting a meeting that is conducive to The Society's wellbeing, then the Committee may extinguish the speaking rights held by said member.
      1. If the Committee believes the discussion of non-Committee members in general to be prohibiting a meeting that is conducive to the Society's wellbeing, then the Committee may extinguish speaking rights for all non-Committee members.
1. # The Officers of the Society
   1. ## General
      1. The officers of the Society shall be the President, the Vice-President, the Secretary the Treasurer and the Equity Officer. These officers shall have the following duties and responsibilities.
   1. ## The President
      1. The Chairperson:
         1. The President shall be chairperson of all General Meetings of the Society, and all Committee meetings, at which they are present.
         1. In the event that the president is absent, the Chairperson shall be the highest ranking officer of the society present at the meeting. The officers in order from highest ranking to lowest are: the President, The Vice President, The Secretary, the Treasurer and the Equity Officer.
         1. If no Officers of the Society are present at a General or Committee Meeting, the meeting shall elect a chairperson.
         1. At all General Meetings and Committee Meetings, the chairperson shall have a deliberative vote, and in the event of an equality of voting, the chairperson shall have the casting vote.
      1. The President shall present a report on the Society’s activities during the preceding year to the Annual General Meeting.
      1. The President shall exercise leadership in relation to the conduct of the Society and its activities during their term of office.
   1. ## The Vice President
      1. The Vice-President shall officiate in the absence of the President.
      1. The Vice-President shall provide support and assistance to the President in the conduct of the Society and its activities.
   1. ## The Secretary
      1. The Secretary shall be charged with the performance of the administrative functions of the Society.
      1. The Secretary shall summon and provide notice of all General Meetings and all Committee meetings, and shall keep minutes of all such meetings. The President also has the power to summon meetings, in which event the secretary need not also summon the meeting.
      1. The Secretary shall maintain records of all correspondence received by and entered into by the Society, and shall attend to all correspondence of the Society as required.
      1. It is the Secretary’s responsibility to ensure that minutes of all meetings and all correspondence are made available to members of the Society if a member so requests.
      1. The Secretary shall keep a register of members of the Society which shall record the names, addresses, and phone numbers of those members.
   1. ## The Treasurer
      1. The Treasurer shall bear responsibility for the financial affairs of the Society.
      1. The Treasurer shall collect all subscriptions and other moneys received by or on behalf of the Society, and shall lodge the same to the credit of the Society at such bank as the Committee shall direct.
      1. The Treasurer shall keep full accounts of all moneys received and expended by the Society.
      1. The treasurer shall at appropriate intervals, update the committee as to the financial position of the society, or present a financial statement upon the request of the committee.
      1. The Treasurer shall prepare a budget, detailing a forecast of incomes and expenses of the society for that given year, to be submitted to the Initial General Meeting of the Society, for its approval.
      1. The Treasurer shall prepare a financial statement at the end of each financial year, detailing the Society’s income and expenditure, providing particulars of any securities affecting any of the property of the Society at the close of the said year, and including a balance sheet supported by a schedule of assets and liabilities. Such financial statement shall be submitted to the Annual General Meeting of the Society, for its approval, whereupon a copy shall be delivered to the Registrar of Incorporated Societies.
   1. ## The Equity Officer
      1. Equity is key to ensuring that debating is accessible and enjoyable for all. The role of the Equity officer is to promote Equity and to ensure that it is respected within the General Society and the Committee.
      1. The Equity Officer will be guided by the Equity Policy set out in the document “Victoria Debating Society Guidelines to the Role of Equity Officer”.
      1. For the avoidance of doubt, the mere fact that the Equity Officer position exists does not detract from the fundamental obligation of all members of the General Society and Committee to respect Equity.
1. # General Meetings
   1. ## Competence and procedure that applies to all general meetings (annual, initial and special):
      1. The Society shall be bound by any decision or resolution properly made by a general meeting of the Society.
      1. All members are entitled to attend and participate in a general meeting
      1. All members shall be given seven days’ notice of a general meeting.  Such notice may be given by personal notification in writing, by email, by post to members’ addresses, by notice in the Students’ Association newspaper, or by a combination of these methods.
      1. No business shall be transacted at any general meeting unless a quorum of members is present.  Ten (10) ordinary members present shall constitute a quorum.
         1. If after the expiration of such time as the chairperson may determine from the appointed time for the commencement of the meeting, there is no quorum present, the chairperson shall adjourn the meeting to a date and time to be determined by those present.
         1. Notice of the new date and time shall be given to all members in the manner for notice of a General Meeting.
         1. If at the resumed meeting there is still no quorum present, those preseshall constitute a quorum, and shall be entitled to transact the business of the meeting, the chairperson shall have discretion to declare that such a meeting lapse.
      1. Upon request of a member the chairperson may determine if the quorum still exists and if not shall adjourn the meeting.
      1. The procedure of a general meeting shall be governed by article 9 and 10 of this constitution.
      1. Votes by proxy shall not be permitted.
   1. ## Annual General Meetings
      1. The Society shall, in each year, hold an Annual General Meeting.
      1. The Secretary or President shall summon the Annual General Meeting to occur during the month of September or October, providing notice in the manner prescribed above to all members of the date, time and venue of the meeting, and of business intended to be transacted.
      1. The ordinary business of the Annual General Meeting shall be:
         1. to confirm the minutes of the preceding Annual General Meeting.
         1. to receive the President’s report and the Treasurer’s Financial Statement;
         1. to elect the officers of the Society, other members of the Committee, the Patron of the Society, and as required any Life Member or Life Members of the Society.
      1. Any election promises made by elected executive officers at the AGM shall be recorded by the outgoing secretary. Such records shall then both be circulated to the incoming Committee and be available on request in electronic form to all those members who ask for them.
   1. ## Special General Meetings
      1. The Secretary or President shall summon a Special General Meeting of the Society forthwith upon the requisition of the Committee or the requisition of not less that five ordinary members. Such requisition shall be addressed to the Secretary and shall set out specifically the business for which the Special General Meeting is required.
      1. The Secretary or President shall provide notice in the manner prescribed to all members of the date, time, and venue of the meeting, and shall set out the business to be considered at the meeting.
      1. At a Special General Meeting only the business set out in the notice convening the meeting shall be considered, and only decisions or resolutions directly related to or arising from such business may be transacted. Provided that the meeting is unanimous it may vote, consider and transact additional business.
1. # Procedure of General Meetings of the Society
   1. ## General
      1. All provisions for General Meetings under this Article (Article 9) may be suspended for a specified time at any meeting with the unanimous consent of all the members present and entitled to vote.
      1. Meetings shall, subject to the presence of quorum, start at the time set out in the notice and shall continue until all business on the agenda has been disposed of.
   1. ## Speaking Rights
      1. Speaking rights shall allow persons to speak at a meeting. All members (non-student, ordinary and life members) shall have speaking rights at a General meeting.
   1. ## The Chairperson
      1. The chairperson of the meeting shall be responsible for the order and conduct of the meeting.
      1. As and when required the chairperson shall rule on the interpretation and application of this article, and on any other disputed questions of procedure relating to the conduct of the meeting.
      1. Any member disagreeing with a ruling of the chairperson may move dissent. The chairperson shall vacate the chair immediately if such motion is seconded. The mover of the motion shall then speak to it, and the chairperson shall reply, after which the motion shall be put to the meeting without further discussion for an immediate decision by a simple majority of those present and entitled to vote. The chairperson shall then reassume the chair and accept the ruling of the meeting.
      1. The chairperson shall be heard without interruption. Should the chairperson rise to speak, any person then speaking shall defer to the chairperson.
      1. Every member desiring to speak shall address themselves to the chair.
      1. Should two or more members desire to speak simultaneously, the chairperson shall call upon the member who in the chairperson opinion, first indicated their desire to speak.
      1. The chairperson may at any time propose the imposition of a time limit on speakers, or on discussion of a particular item on the agenda.
   1. ## Motions
      1. When a motion has been proposed and seconded by members of the Society, the chairperson shall place it before the meeting for discussion.
      1. If required to do so by the chairperson, the proposer of the motion shall submit it in writing.
      1. A Motion before the meeting may be reworded by the mover subject to the leave of the meeting.
      1. A Motion before the meeting may only be withdrawn by its mover and by the leave of the meeting.
      1. The proposer of a motion or an amendment shall have a right of reply before such motion or amendment is voted upon.
      1. A Motion shall be decided by voice, provided that any member at the meeting may require a show of hands, and further provided that any member at the meeting may require a secret ballot to be taken.
      1. A Motion that has been resolved by the meeting may be rescinded or recommitted by a majority vote of the meeting.
   1. ## Amendments of Motions
      1. A motion may be amended by its mover with the consent of the seconder.
      1. An amendment to a motion may be moved and seconded by any two members, provided that it does not constitute a direct negation of the original motion, and further provided that it is relevant to the motion to which it is moved.If the proposer and seconder of the motion accept the amendment, then the motion as amended shall become the substantive motion. If they do not accept the amendment , the amendment shall be put to the meeting before the motion, and if carried, the motion as amended, shall be put to the meeting.
      1. Whilst an amendment is before the meeting discussion shall be confined to that amendment. No further amendment shall be proposed until the amendment before the meeting has been disposed of, although further amendments may be foreshadowed.
   1. ## Procedural Motions
      1. The discussion of a particular matter may be interrupted upon one of the following formal motions being proposed and seconded by members of the Society.
      1. The motion “that the question be now put”, provided that it is accepted by the chairperson, who shall have discretion to reject it, shall be put immediately without amendment of discussion. If carried, the motion or amendment in hand shall be put to the vote immediately, subject only to the mover of the motion’s right of reply.The chairperson may of their own volition put the question if they regard that adequate discussion has taken place.
      1. The motion “that the question be not now put”, which may not be proposed whilst a member is speaking, or during the discussion of an amendment, shall be open to discussion together with the original motion, provided that it is accepted by the chairperson, who shall have discretion to reject it. If carried, the original motion shall not be dealt with further at the meeting. If lost, the original motion shall be put to the vote immediately, subject only to the mover of the motion’s right of reply.
      1. The motion “that the meeting do proceed to the next business”, which may not be proposed whilst a member is speaking, shall be put immediately without amendment or discussion, provided that it is accepted by the chairperson, who shall have the discretion to reject it.
      1. The motion “that the question do lie upon the table”, which may not be proposed whilst a member is speaking, shall be open to discussion by the meeting, provided that it is accepted by the chairperson, who shall have discretion to reject it. If carried, the question shall not be further discussed, until it is resolved that it be “taken from the table”.
      1. The motion “that the debate or meeting now be adjourned”, which may not be proposed whist a member is speaking, and which may not be rejected by the chairperson, shall be open to discussion’ although only amendments as to time and/or place shall be permitted. The motion shall take precedence over other business before the meeting, except points of order.
      1. The motion “that the speaker no longer be heard”, provided that it is accepted by the chairperson, who shall have discretion to reject it, shall be put without amendment or discussion.
      1. Upon a motion of no confidence in the chair being proposed and seconded, the chairperson shall vacate the chair immediately. The motion shall be open for discussion, and the chairperson may only resume the chair upon the motion being lost. If the motion passes, the next in the hierarchy, as mentioned in article 7.2(a) shall take position as chairperson.
   1. ## Points of Order and Information
      1. Any member may raise a point of order, by stating that they do so, which shall take precedence over all other business, except during the act of voting unless the point of order relates specifically to the procedure of that vote.
      1. A Point of order should concern a breach of the Constitution which may include a breach of these Standing Orders.A point of order should be raised immediately such breach occurs.
      1. The chairperson may open brief discussion on the point of order but new matter shall not be debated.
      1. The chairperson’s ruling on a point of order shall be final, and may not be discussed unless dissent is moved.
      1. Points of Information may be raised by any member, provided that they may only consist of information offered to or asked of the chairperson, and shall contain no argument.
   1. ## Committee of The Whole
      1. At any time the meeting may resolve itself into a committee of the whole. Except for any motions which are carried, the proceedings of the committee of the whole shall not be reported or recorded.
   1. ## MATTERS NOT DEALT WITH
      1. Any matter not dealt with under Article 9 regarding the procedure of a General Meeting shall be determined by a ruling of the chairperson, which shall be given in accordance with the principles of debate governing the New Zealand House of Representatives.
1. # Elections
   1. ## General
      1. The officers of the Society, other members of the Committee, and the Patron of the Society, shall be elected at the Annual General Meeting, to hold office for the next calendar year.
      1. All procedures under Article 10, apart from anything under article 10.1 may be suspended for a specified time at any meeting with the unanimous consent of all the members present and entitled to vote.
      1. Each member of the Society present and entitled to vote at a meeting at which elections are held, shall have one vote in such elections.
      1. The officers of the Society shall be elected first, in order of precedence, after which the other members of the Committee shall be elected, after which the Patron of the Society shall be elected.
   1. ## Preliminary Arrangements
      1. Prior to the holding of elections, two scrutineers shall be elected by the meeting, who upon accepting office shall be disqualified from nominating candidates or being candidates in such elections. However, scrutineers, if they are members, retain the right to vote.
      1. Every member of the Society present and entitled to vote shall be issued with a single ballot paper for each office to which elections are to be held.
   1. ## Nominations
      1. Any member of the Society may nominate themselves or any other member to any office on the committee provided that the nominee is a student at the Victoria University of Wellington. Seconders shall not be required.
      1. Nominations by Chairperson
         1. The chairperson of the meeting may only propose a nomination upon vacating the chair.
         1. The chairperson shall vacate the chair during any election in which they have nominated a candidate.
         1. The chairperson of the meeting shall vacate the chair immediately upon accepting nomination for any office, for the duration of such election.
      1. All candidates for election shall have the right to decline nomination, and may withdraw such nomination at any stage prior to the vote being taken.
      1. In respect of each office on the Committee in turn, the chairperson of the meeting shall call for nominations to such office. The chairperson shall firstly read any written nominations which have been received by the chairperson or the Secretary prior to the opening of the meeting. The chairperson shall then invite further nominations from the meeting. After closing nominations, the chairperson shall invite candidates to address the meeting in the order in which they were nominated. The vote shall then be taken to determine election to such office.
   1. ## Mode of Voting
      1. A secret ballot shall be held for all elections to office on the Committee.
      1. Members shall exercise their vote according to the following procedure:
         1. In order to vote for a candidate, the member shall write the name of that candidate on the appropriate ballot paper, and deposit such ballot paper in the sealed container under the supervision of the scrutineers, when requested to do so by the chairperson.
         1. If a member has no confidence in all of the candidates seeking election to office (or if there is only one candidate, in that candidate) that member shall write no confidence on the appropriate ballot paper, and similarly deposit it in the sealed container as hereinbefore provided.
         1. If a member does not wish to exercise a vote in the election for any office, that member shall leave the appropriate ballot paper blank and shall similarly deposit it in the sealed container as hereinbefore provided.
      1. In respect of the elections of officers of the Society, members may only vote for one candidate for each office.
      1. In respect of the election of other members of the Committee, members may vote for up to as many candidates as there are positions to be filled.
   1. ## Officers of the Committee: Method of Election:
      1. In respect of the election of officers to the Society, a candidate shall only be elected to office upon obtaining a majority of the votes cast for all candidates. This statement shall be interpreted in the following manner:
         1. In the event of there being only one nomination for any office, such candidate in order to be elected shall be required to receive a higher number of votes in their favour than the number of no confidence votes that are cast.
         1. In the event of there being two nominations for any office, the candidate who receives the greater number of votes in their favour shall be elected. Provided that such candidate in order to be elected shall be required to receive a higher number of votes in their favour than the number of no confidence votes that are cast.
         1. In the event of there being more than two nominations for any office, a candidate who receives a higher number of votes in their favour than the combined number of votes cast for other candidates shall be elected to such office upon the first ballot. In the event of no such candidate being elected, a second ballot shall be held between the two highest polling candidates in the first ballot. The candidate who receives the greater number of votes in their favour in the second ballot, shall be elected. Provided that such candidate in order to be elected shall be required to receive a higher number of votes in their favour in the second ballot than the number of no confidence votes that are cast in such ballot.
         1. In the event of there being no nomination for any officer no candidate being elected to such office, such vacancy shall be filled in accordance with the procedure prescribed in article 9 of the constitution. Provided that the Committee shall always have the right to name an interim appointee to such office for the period prior to such procedure being completed.
   1. ## Other Committee Members: Method of Election:
      1. In respect of the election of other members to the Committee, candidates shall be ranked from the highest polling to the lowest polling, and up to as many candidates as there are positions to be filled, shall be elected, according to the order of such ranking.
      1. In the event that a majority of all votes cast in such election are no confidence votes, no candidate shall be elected to the Committee.
      1. In the event of there being fewer candidates than there are positions to be filled, or no candidate being elected to the Committee, such vacancy or vacancies may be filled in accordance with the procedure proscribed in article nine of the Constitution.
   1. ## The Counting Of Votes
      1. Votes cast shall be counted by the two scrutineers elected. Each candidate for election or their nominee may witness the counting of votes for the election in which they seek office. No other person shall be present at the counting of votes.
      1. Prior to the result of the election being declared, each candidate or their nominee shall have the right to protest against the scrutineers’ interpretation of the counting of votes, in the first instance to the scrutineers’ and in the second instance to the chairperson of the meeting, who may order a recounting of the votes, and if necessary the election of new scrutineers.
      1. Scrutineers counting:
         1. The scrutineers shall ascertain that the number of votes cast combined with the number of blank ballots deposited is equal to the number of members present and entitled to vote. The meeting shall forthwith be notified of any disparity , subsequent to which any member may require a fresh ballot
         1. The scrutineers shall ascertain that each ballot paper has been correctly completed. An incorrectly completed ballot paper shall have no effect.
         1. In the event of an equality of votes for any office of the Committee:
            1. The chairperson shall request a re-ballot for such position
            1. In the event that such re-ballot also result in an equality of votes, the chairperson shall exercise their casting vote.
   1. ## Election Of Patron
      1. Any member of the Society may nominate any person for the office of Patron of the Society. Such election shall be determined upon a show of hands, provided that any member at the meeting may require a secret ballot to be taken.
1. # Finances
   1. ## General
      1. The Society’s financial year shall commence on the first day of January and end on last day of December of the same year.
      1. The membership subscription payable under Article four shall be such amount as may be determined by the Committee.
      1. The trustees of the Society’s bank account, and any other account held in the name of the Society, or held in relation to any activities of the society, shall be the financial sub-committee.
      1. Any officers of the society may be appointed a trustee of any bank accounts held in the society’s name, or held in relation to any activities of the society, upon the approval of the committee.
   1. ## The Finance and Administration Sub-Committee
      1. The Finance and Administration Sub-Committee (F&A) is comprised of the President, Vice-President and Treasurer.
      1. F&A are responsible for the smooth running of the financial management of the Society and are responsible for advising the Committee on legal liability issues.
      1. F&A may approve normal expenditure items up to a value as is from time to time fixed by an annual general meeting and until so fixed up to a value of $150 in the aggregate if they are unanimous.
      1. Any expenditure item so approved must be further approved at the next available meeting of the Committee and may be overturned by a majority motion of the Committee. If expenditure is overturned, the F&A committee will pay the Society the expenditure. F&A have the right to appeal to an SGM called for the purposes of ratifying said expenditure. The decision of the SGM shall by binding and final.
      1. F&A members must be the three signatories on any bank account, of which any two may sign on the account.
1. # Sub-Committees
   1. ## Sub-Committees
      1. The Committee shall have the power to appoint from time to time, sub-committees for any purpose connected with the management of the affairs of the Society, and to delegate to them such authority as the Committee is itself entrusted with, subject to the following reservations:
         1. A member of a sub-committee must be appointed by a majority motion of the Committee and must be renewed as a member of the sub-committee annually.
         1. A member of a sub-committee may be removed by a majority motion of the Committee.
         1. A member of a sub-committee must be a member of the Society.
         1. The Committee may alter the conditions of clauses 11.1(a)(i)(ii) & (vi) upon a two-thirds majority of the Committee for any particular sub-committee. This must be set out in the delegation document as outlined in clause 11.1(a)(vii).
         1. A sub-committee may not undertake loans unless it is approved by a motion of the Committee. Permission must be sought for each specific loan.
         1. A sub-committee may not enter into other contractual arrangements except with the permission of three of the President, Vice-President, Treasurer and Secretary. If any of the above people are unavailable the Committee may appoint representatives as they see fit to act on their behalf for the purposes of this clause.
         1. A sub-committee must have a written delegation document approved by the Committee which sets out the purpose of the sub-committee and the powers that have been delegated to it.
1. # Registered Office and Common Seal
   1. ## General
      1. The registered office of the Society shall be at the Victoria University of Wellington Students’ Association, or at such other place as the Committee may determine.
      1. The Common Seal of the Society shall bear the name of the Society and shall be kept in the custody of the Secretary. The seal shall be used over the signatures of the President and Secretary as and when directed or authorised by the Committee, provided that if the President or Secretary is not available, some other member of the Committee may be authorised by the Committee to sign in their place.
1. # Constitution Interpretation
   1. ## General
      1. If any question of interpretation of this Constitution shall arise, such question shall be referred to the Committee for a ruling.
      1. Should dispute still remain the issue shall be referred to the Victoria University of Wellington Students’ Association (VUWSA), for resolution.
      1. If VUWSA is a party to the dispute then mediation between VUWSA and the Committee, as the Society’s representative, shall be entered into. Should this fail to resolve the dispute then the Victoria University Council shall be asked to make a binding ruling.
1. # Amendment of this Constitution
   1. ## General
      1. The provisions of this Constitution may be altered, added to, or rescinded, either in their entirety or in part, at a General Meeting of the Society, provided that notice of such amendment is given to all members seven days before the date of such meeting, and further that such amendment is approved by at least two-thirds majority of those present and entitled to vote.
      1. No addition to or alteration of the non-profit aims, personal benefit clause or the winding up clause shall be approved without the approval of Inland Revenue.
      1. The provisions and effect of this clause shall not be removed from this document and shall be continued and implied into any document relating to this document.
1. # Winding Up
   1. ## General
      1. The Society shall be dissolved if at any General Meeting, of which due notice of this intent is given, a resolution to this effect is carried by a majority of members present and voting. Provided that a Special General Meeting shall be called for the purpose of confirming or rejecting such resolution, such meeting not to be called earlier that thirty days after the date of the meeting at which such resolution was carried, and such dissolution shall not be valid unless and until confirmed by a seventy five per cent majority of such members present and entitled to vote at such meeting.
      1. If upon winding up or dissolution of the Society there remains after satisfaction of all debts and liabilities any property whatsoever the same shall not be paid to or distributed among the members of the Society but shall be given or transferred to or vested in the Victoria University of Wellington, to be held upon trust for any similar society which may be established to foster and promote debating and public speaking among students at the Victoria University of Wellington, or to some other charitable purpose associated with the Victoria University of Wellington.
1. # Trialling Method
   1. ## General
      1. Trials to select teams to represent the Society shall be conducted in teams not determined by trialists
      1. The Committee shall determine Selectors for trials
      1. Selectors shall determine which trialists represent the Society, in which teams.
   1. ## Method of Selection
      1. The Selectors shall evaluate trialists on the basis of factors determined by the Committee for the specific trial in question. The Committee may give selectors direction about how they expect the factors to be weighed or applied. Factors may include, but are not limited to:
         1. Performance in trials;
         1. Past performance;
         1. Potential for development; and,
         1. Preferences expressed by trialists to be selected in teams including (or not including) other trialists.
      1. For the avoidance of doubt the trialling method described above applies but is not limited to the following tournaments:
         1. New Zealand Universities Prepared Debating Championships;
         1. New Zealand Universities Impromptu Debating Championships;
         1. Australasian Intervarsity Debating Championships; and,
         1. World Universities Debating Championships.
1. # Plunket Medal
   1. ## PREAMBLE
      1. The Plunket Medal Oratory Contest was endowed by the then Governor of New Zealand, Lord Plunket, in 1905, and is New Zealand’s oldest oratory contest.
   1. ## ORGANISATION
      1. The Society can choose in each year whether to organise and conduct an oratory contest for the Plunket Medal.
      1. Date of the Contest
         1. The Committee shall before the end of the first term determine the date upon which the contest shall be held.
         1. The Secretary shall publish the date of the contest, and the requirements for entry,
         1. in order that as many students as possible should be informed of and invited to consider participation in the contest.
      1. Contest Convenor
         1. The Committee shall by the beginning of the second term decide whether to appoint a Plunket Medal Contest Convenor, who upon accepting office shall be disqualified from entering the contest in that year. The Convenor shall organise the contest, including publicity arrangements, the printing of a programme, liaison with contestants and judges, preparation and serving of a supper. The Committee may at its discretion appoint an Assistant Convenor, to assist in the exercise of these functions.
         1. The Convenor shall exercise these said functions subject to supervision by the Committee. They shall consult with the Committee to the fullest extent possible.
   1. ## ENTRY REQUIREMENTS
      1. The contest shall be open to any person who is enrolled as a student at the Victoria University of Wellington.
      1. Past winners of the contest shall be ineligible to enter again.
      1. Any person wishing to take part shall be required to make application which shall include the title and subject of the oration, to be received by the Secretary no later than two weeks before the date of the contest. The Committee shall have the discretion to vary this provision in relation to a specific contest. The Secretary shall notify the convenor of all such entries in the contest, and no later than one week before the date of the contest, shall provide all entrants with a copy of this article.
      1. The Committee may reject any entry received, if it considers the subject of the oration frivolous or likely to bring the contest and the Society into disrepute, provided that the grounds for such decision are stated in writing to the intending contestant within one week of the receipt of the entry, and further provided that such contestant be allowed the opportunity to select a different subject which may be approved by the Committee.
      1. In the event of more than eight entries in the contest being received, a preliminary round shall be conducted in order to select eight finalists, provided that the Committee shall have the discretion to vary this provision in relation to a specific contest. The date of a preliminary round shall be notified to all contestants at least one week before such contest is held.
   1. ## CONTEST ARRANGEMENTS
      1. The speaking order of contestants shall be determined by lot.
      1. Contestants shall wear formal dress, and academic gowns provided by the Society.
      1. The contest shall, where possible, be chaired by the winner of the previous year’s contest.
      1. The patron of the Society shall, where possible, be invited to present the Plunket Medal to the winner of the contest.
      1. Speaking Times:
         1. Contestants shall be required to speak for between ten and twelve minutes.
         1. Any contestant who speaks for less than ten minutes shall be disqualified, and the judges shall be instructed to deduct marks for any contestant who speaks for more than twelve minutes.
         1. The timekeeper shall sound a warning bell at ten minutes, and two rings of the bell at twelve minutes.
      1. The use of notes by contestants shall not be permitted.
      1. Interjections from the audience shall not be permitted.
   1. ## ORATION
      1. Contestants’ speeches shall be in the form of an oration.
      1. Each oration shall be any original work, which shall not have been previously delivered in an oratory contest.
      1. The attention of contestants and judges shall be specifically directed to the first annexe hereof, concerning the nature of an oration.
   1. ## ADJUDICATION
      1. The contest shall be adjudicated by three judges, selected at the discretion of the Committee, who shall be announced, where possible, no later than one month before the date of the contest. The Committee shall nominate one of the judges as chairperson of the adjudication panel.
      1. The Secretary shall -provide the judges with a copy of this Article as soon as practicable after their appointments, and shall cause additional copies to be provided at the contest.
      1. After the last contestant’s oration, the judges shall retire privately to consider their decision, which shall be determined by majority vote.
      1. The chairperson of the panel shall announce the winner of the contest.
      1. The judges shall have regard to the second annexe hereof in arriving at their decision.
      1. The attention of contestants and judges shall be specifically directed to the second annexe hereof, concerning suggested adjudication criteria.

{{< extraCSS >}}
   #preamble {
      white-space: pre-wrap;
   }
   ol h1, ol h2 {
      font-size: 1em; 
      scroll-margin-top: 15px;
   }
   .hash-link {
      margin-top: 0;
      background: var(--normal-background);
   }
{{< /extraCSS >}}