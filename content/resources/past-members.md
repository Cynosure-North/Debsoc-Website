---
layout: resource
title: Past Members
cover_image: /uploads/vanitas-still-life-pieter-claesz.jpg
publishDate: 2023-11-09
summary: Debsoc has a great number of past members who have gone on to top
  various fields and become well known names around New Zealand and the world.
---
Debsoc has a great number of past members who have gone on to top various fields and become well known names around New Zealand and the world. A number of the most noteworthy former Debsoc members are listed below.

___

[Hon Christopher Finlayson](http://en.wikipedia.org/wiki/Chris_Finlayson) – Attorney-General (2008-2017), Minister of Treaty of Waitangi Negotiations (2008-2017 and Minister for Arts, Culture and Heritage (2008-2017)

[Hon Bill English](http://en.wikipedia.org/wiki/Bill_English) – Prime Minister (2016-2017), Deputy Prime Minister and Minister of Finance (2008-2016)

[Hon Tim Groser](http://en.wikipedia.org/wiki/Tim_Groser) – Minister for Trade (2008-2015), Minister for International Climate Change Negotiations (2008-2015)

[Russell Fairbrother](http://en.wikipedia.org/wiki/Russell_Fairbrother) – Labour Party MP

[Hamish Hancock](http://en.wikipedia.org/wiki/Hamish_Hancock) – former National Party MP and now Crown Counsel.

[Raybon Kan](http://en.wikipedia.org/wiki/Raybon_Kan) – Comedian, [Sunday Star-Times](http://en.wikipedia.org/wiki/Sunday_Star-Times) columnist

[John Platts-Mills](http://en.wikipedia.org/wiki/John_Platts-Mills) QC – Rhodes Scholar and UK MP

Judge Peter Boshier – Principal Judge of the Family Court of New Zealand (2004-2012), [New Zealand Chief Ombudsman](http://en.wikipedia.org/wiki/New_Zealand_Chief_Ombudsman) (2015-present)

[Justice Stephen Kos](https://en.wikipedia.org/wiki/Stephen_K%C3%B3s) – President of the Court of Appeal (2016-2022), Justice of the Supreme Court (2022-present)

Sir [Matthew Oram](http://en.wikipedia.org/wiki/Matthew_Oram) – [Speaker of the New Zealand House of Representatives](http://en.wikipedia.org/wiki/Speaker_of_the_New_Zealand_House_of_Representatives) (1950-1957)

Rt.Hon. Sir [Humphrey O’Leary](http://en.wikipedia.org/wiki/Humphrey_O%27Leary) KCMG – [Chief Justice of New Zealand](http://en.wikipedia.org/wiki/Chief_Justice_of_New_Zealand), (1946-1953)

Rt.Hon. [Edmund (Ted) Thomas](http://en.wikipedia.org/wiki/Edmund_Thomas_%28jurist%29) – acting Justice of the [Supreme Court of New Zealand](http://en.wikipedia.org/wiki/Supreme_Court_of_New_Zealand)

Sir [Brian Elwood](http://en.wikipedia.org/w/index.php?title=Brian_Elwood&action=edit&redlink=1) – [New Zealand Chief Ombudsman](http://en.wikipedia.org/wiki/New_Zealand_Chief_Ombudsman) (1994-2003)

Sir [David Smith](https://en.wikipedia.org/wiki/David_Smith_(judge)) – Justice of the [Supreme Court (now the High Court)](http://en.wikipedia.org/wiki/High_Court_of_New_Zealand) and Chancellor of [Victoria University](http://en.wikipedia.org/wiki/Victoria_University_of_Wellington)

[Oswald Chettle Mazengarb](http://en.wikipedia.org/wiki/Oswald_Chettle_Mazengarb) QC – Prominent Barrister and author of the [Mazengarb Report](http://en.wikipedia.org/wiki/Mazengarb_Report)

Peter Biggs – Head of Colenso and former Chair of Creative NZ

Gerard Curry – Senior litigation partner in [Russell McVeagh](http://en.wikipedia.org/wiki/Russell_McVeagh) and lawyer for [Alain Mafart](http://en.wikipedia.org/wiki/Alain_Mafart) and [Dominique Prieur](http://en.wikipedia.org/wiki/Dominique_Prieur)

 

The Society has also had a number of members go on to be Rhodes Scholars to Oxford University. This is New Zealand’s most prestigious scholarship. These members are:

Malcolm Birdling

Chelsea Payne

Alexandra Gillespie

John Platts-Mills
