---
layout: resource
title: POIs
cover_image: /uploads/logo.png
publishDate: 2023-11-09
summary: This is POI Guy, our mascot.
---
Points of Information (POIs) can be an effective way to derail an opposing speaker, or just waste their time.

___

POIs allow a member of the team opposing the current speaker to stand and ask for permission to offer a POI. This is usually done simply by saying “on that point”, “point of information” or simply “madam/sir/speaker”. The speaker on the floor is then allowed to accept or decline the POI. If declined, the debater offering the point must sit down immediately without speaking. If accepted, the debater is invited to ask a short question of the current speaker. Points should be short, witty and to the point, and should not be any longer than a couple of sentences or 15 seconds. It is important to note that the speaker at the front has control of the floor the whole time, and can ask a debater to sit down, or instruct that they have understood the point without need for further explanation. Following the point, the speaker is expected to answer the point in any fashion, so as to defend their case.

POIs can be offered during any of the substantive speeches, but not during Leader’s replies. During these speeches they can be offered between the end of the first minute, and the start of the last minute. For this reason there is one bell given after 1 minute, as well as another at 1 minute before the end of the speech. Speakers are expected to accept around two POIs during their speech, however opposition teams are expected to offer many more than that.
