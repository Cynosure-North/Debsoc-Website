---
layout: resource
title: Tournaments 101
cover_image: /uploads/jousting.jpg
draft: false
publishDate: 2024-04-05
summary: Tournaments are the best part of debating, but there can be a lot going on.
---
{{< contents >}}

Tournaments are awesome. I cannot recommend them enough.

The single most important factor is that you have the lyrics to [Victoria](https://youtu.be/uPA7KjJxn9k) memorised.

# Logistics

## Schedule

Majors normally run from Friday evening until Monday night.

Opening night socials are usually at a bar, from 7ish until 11ish.
They're a great opportunity to have chill drinks and meet people from other campuses.

On Saturday briefing is between 8:30 - 9:30am depending on the tournament. Make sure to check the event page and coordinate with the rest of your squad. Lunch time varies depending on how badly the tournament is tramming. Debating can wrap up any time from 4 - 7pm. Usually people will get a squad dinner and have a chill hangout.

On Sunday things run basically the same. Sometimes you can get a little more sleep because there's no briefing, but don't count on this. After debating everyone will head to a restaurant for break night BYO. After that we will sing Victoria, and then go to kick ons.

Break night is where the breaking judges and teams are announced, along with any trainees accrediting. Trainees can also be called back to trainee on the semi finals.

On Monday the semis will be run first. Then POC forum, WGM forum, and council (these often tram). After that is the finals and prizegiving. The final event of the tournament is closing night drinks, which is like opening night but rowdier.

The forums are run autonomously, meaning it is politely requested that if you are not a person of colour or a woman and/or gender minority you don't attend. If you are though, it is strongly encouraged that you attend the respective forum(s). There will be food, and they're a great opportunity to reflect on the tournament, and debating more widely. We're always working to make it fun for everyone and without attending it's hard to make improvements.

Council is where the campuses come together to discuss the tournament, consider changes to debating, and do a lot of arguing. They are often entertaining if you like inane debating politics. Only campus presidents have speaking rights, but you can ask your president to grant you rights temporarily.

People head home on Tuesday morning.

For tournaments other than majors, such as Thropy, things run a bit differently. There are fewer in rounds, usually one day less. So it's like the schedule for a major with no Saturday. Forum and council aren't run, so things wrap up earlier, usually by lunch on Sunday so people can get home by the evening. The socials may be less alcoholic and more informal.

## Transportation

If the tournament is in the North Island, except Auckland, we will road trip there. Exec will organise a seat for you, just make sure to turn up on time.

For everywhere else you will need to sort flights out yourself.
People generally fly in Friday afternoon to give time to settle in and do some touristing, and fly out Tuesday morning. It's not worth booking flights on Monday in case you break, and the flights are more expensive.

Flights are cheaper the further in advance you book, but tournaments dates aren't always set in stone.

## Accommodation

Exec will sort accommodation. Usually this is a hotel room with other debaters. You will know who you are rooming with in advance, and you can always ask to change room.

Remember your manners, always knock and you'll be fine.

It's worth making a group chat for your room.

You're also welcome to sort out your own accomodation, and not pay for the hotel.

## Food

Lunch is provided on days with in rounds. You have to sort the rest, but discuss with your squad because they'll have plans.

There will be a field on the tournament registration form for any dietary requirements you have.

## Packing

You are an adult who will remember toothpaste. Make a checklist if you forget things and update it when you realise what you forgot, for next time.

Paper and pens are important.

Other useful things worth considering:

* Snacks. Avoid sugary ones or you will crash at the worst time.
* Backup pens.
* Phone charger
* Water bottle
* Multibox. Useful if your roommates try to take all the plugs.

## Price

There is subsidy available for everyone who meets practice requirements.
We may also be able to help by spreading out payment.

The actual price varies depending on the time and location of the tournament.

Rough estimates as of early 2024:

* $60 - Rego for majors or
  $30 - Rego for other tournaments
* $400 - Flights or
  $80 - Fuel
* $250 - Accommodation
* $60 - Other costs (food, energy drinks, alcohol, etc)

# Equity & Safety

For the duration of the tournament including all socials, equity policy applies.

See our Equity Guide (TODO, for now just remember to make sure others have fun and feel safe, they will do the same for you) for all the details, and remember that you can always talk to your campus equity officer or any member of exec as well. Even if it's minor it's always worth letting someone know.

This shouldn't need to be specified, but here we are.

Remember to use common sense.

Stick with the rest of the squad and if you're heading off make sure to tell people where you are. It's fine to go off and see the sights or hang out with friends but if the rest of the squad is worried that's bad.

Consent is important

## Alcohol

Don't pressure others to drink, and don't let anyone else pressure you. If you feel uncomfortable there are lots of people who you can talk to for help.

There are always people who don't drink, and lots who don't drink much. Don't feel like you need to drink to participate.

If you're under 18 you are not allowed to buy alcohol, even if you tell them you're a debater.

## Emotions

Tournaments are a lot. Remember that this is a game we play.

There should be a quiet room at every tournament where you can go and clear your head. If you aren't able to find it feel free to reach out to someone.

Even if you have a disastrous speech, we've all been there. Take some time to cool off, then come and hang out. There will be other tournaments.

# General advice

Remember to socialise with people from other campuses. The NZ debating community is full of amazing people, and you can see the people from your campus on club night.

If you're used to schools debating, this is a lot more chill. You don't have to take it seriously. You can focus on having a good time and there will be others aiming for the same.

Team spirit is extremely important in debating. If you don't know your team that well consider meeting them in advance to get to know each other better. You can also ask to speak together on club night.

Make sure to sleep well in advance. Sleep is vital to speak well and have fun. You won't get enough at the tournament so prepare in advance.

Drink water. You're probably not drinking enough, and debating is dehydrating.

# Specific advice

If the tournament is over Easter (as Joynt often is) you will need to buy wine for the BYO in advance.

The conveners will let you know if you can bring something other than wine to the BYO. It is rare though, so I suggest finding a white that isn't too disagreeable.

If it's a Dunedin tournament remember to book a shuttle from the airport as it's extremely far out of town and there are no buses. Taxis are expensive. It's cheaper if you book in a group.
