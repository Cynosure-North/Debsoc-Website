---
layout: resource
title: Tournaments We Attend
cover_image: https://uploads5.wikiart.org/images/pierre-auguste-renoir/ball-at-the-moulin-de-la-galette-1876(1).jpg
draft: false
publishDate: 2024-11-04
summary: Tournaments are great if you're competitive, but that's not the only
  way to enjoy them.
---
{{< contents >}}

# New Zealand Tournaments

## Joynt Scroll

**Format:** [Limited Preparation](/resources/guide-to-limited-preparation-format/).

**Location:** The location of the tournament rotates through the universities: Auckland -> Hamilton/Waikato -> Wellington/Vic -> Christchurch/Canterbury -> Dunedin/Otago -> Auckland.

**Time of Year:** Usually held over Easter weekend in April.

**Speaker Eligibility:** Any member of the society. We hold trials for teams. 

**Vibes:** Can be an intense tournament, with long debates and long days. But that challenge is a great way to start the year. It's also good to see all your friends from other unis after the summer.

## Officers' Cup

**Format:** [Impromptu](/resources/guide-to-impromptu-format/).

**Location:** Rotates like Joynt but 3 steps behind, i.e. if Joynt is in Dunedin, Officers' will be in Hamilton. 

**Time of Year:** August.

**Speaker Eligibility:** Any member of the society. We hold trials for teams. 

**Vibes:** Some say Officers' is the most fun tournament of the year, in part because the format [](/resources/guide-to-officers-format/)creates fast and lower-stakes debates, in part because by the August break people are ready for a weekend away. 

## Thropy (North Island Novice Tournament)

**Format:** [Limited Preparation](/resources/guide-to-limited-preparation-format/). 

**Location:** Traditionally Thropy alternated between Hamilton and Palmerston North, but there is talk of setting it in Tūrangi every year.

**Time of Year:** May. 

**Speaker Eligibility:** Thropy is open to speakers who have debated at fewer than 2 majors (Officers' or Joynt). If you have spoken in the finals, or won the finals (depending on the Adj Core's discretion) you may not be eligible.

**Vibes:** Because senior members of the societies can't speak, they usually opt to trainee or judge. For junior members it's an opportunity to meet debaters from other campuses. Everyone speaking is new, so the barrier to entry is low and the speeches are fresh.

## NZWGM (New Zealand Women and Gender Minorities')

**Format:** [Limited Preparation](/resources/guide-to-limited-preparation-format/).

**Location:** Varies depending on which campus bids to host it.

**Time of Year:** Early February.

**Speaker Eligibility:**

Must be a WGM, teams must have at least one novice. The novice rules are as follows:

1. If you are a first or second year you are a novice.
2. If you have been to fewer than 2 majors you are a novice.
3. If you are in your 3rd year or beyond, and have been to 2 or more majors you are *not* a novice.
4. Anything beyond that is discretionary.

**Vibes:** NZWGM is specifically focused on being a fun and friendly space. Many members say it's their favourite tournament because of how different it is -- fewer boozy socials, more friendship bracelets.

# International Tournaments

## Australs (Australasian Intervarsity Debating Championships)

**Format:** [Australs](/resources/guide-to-limited-preparation-format/#australs).

**Location:** Varies within the Asia Pacific depending on what universities bid to host.

**Time of Year:** June/July.

**Speaker Eligibility:**  Any currently enrolled university student. 

**Vibes:** Australs is in the middle of the academic year, usually over the break. Often it takes place in a tropical getaway. These facts combine to create a week long party. It's usually closer than Worlds, so Vic often sends multiple teams. Every year a Vic member (or a substitute from another NZ campus if we don't attend) delivers a speech on why debating is important, and presents the Jock Fanselow Cup to the best speaker of the Grand Final. Jock was a Vic member and the first person to win Australs twice, and the cup is in his memory.

## AWGMDC (Australasian Women & Gender Minorities' University Debating Championships)

**Format:** [BP](/resources/guide-to-british-parliamentary-format/).

**Location:** Like Australs the location of the tournament varies within the Asia Pacific, Vic tends to attend online.

**Time of Year:** September.

**Speaker Eligibility:** Any currently enrolled university student. 

Must be a WGM, teams must have at least one novice. The novice rules are as follows:

1. If you are a first year you are a novice.
2. If you have never debated at Australs or Worlds you are a novice.
3. If you have spoken at more than one Australs or Worlds you are *not* a novice.
4. If you have broken at Australs or Worlds you are *not* a novice.
5. Otherwise you are a novice.

**Vibes:** Like a mix of Australs and NZWGM. Vic has historically done well at AWGMDC and we plan to continue doing well.

## Worlds (World Universities Debating Championship)

**Format:** [BP](/resources/guide-to-british-parliamentary-format/).

**Location:** Varies depending on bid, anywhere in the World.

**Time of Year:** Typically 27th December - 5th January. 

**Speaker Eligibility:** Currently enrolled university students, and people who graduated the term immediately prior to the tournament (not including summer uni). No cross-institution teams. we hold trials for teams, often early in the year.

**Vibes:** WUDC is the premier debating event, but you shouldn't go for the debating. The wide variety of judging and speaking styles is exciting if you're there for fun and novelty, and terrible if you expect to sweep everything. Also there are 400-500 teams so you do the math. You should go there for the travel, and you should travel before the tournament so you can acclimatise and connect with your team. You should also go because being at a tournament with 1000 other debaters (potential friends!) is an incredible experience. Everything you love (and hate) about majors, but more.

# Other tournaments

## NZBP

NZBP is the premier [British Parliamentary](/resources/guide-to-british-parliamentary-format) debating tournament in NZ. Held at the end of November/start of December in Wellington, it attracts a lot of former members keen to relive the glory days. Usually those members are very strong speakers, so be prepare to fight for wins. Often people form pro-am teams, but it's not a strict requirement.

## Auckland IV / South Island IV (SIIV)

These are BP tournaments run by other NZ universities over the summer. Auckland IV is in person in Auckland, and SIIV is online.

## Claytons

Claytons is the South Island Equivalent of Thropy. Its atmosphere is much more relaxed and there's usually at least one round with ridiculous motions. If you want to attend, make friends with a South Island debater and wheedle them for an invite.

## Victoria Shield / Parliamentary Shield

We used to run these as open tournaments that the public, usually former debsoc members, could participate in. These days it's just current members who turn up. Held the week before Joynt and Officers' respectively, they're used to dial in the team dynamic.
