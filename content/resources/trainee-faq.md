---
layout: resource
title: Trainee FAQ
cover_image: /uploads/f05.png
draft: false
publishDate: 2024-04-06
summary: Becoming a judge can be a difficult and confusing process. With this
  information you’ll be prepared to accredit.
---
{{< contents >}}

# Who is accrediting me?

The accreditation process is run by super adjudicators, namely, people who have been on NZUDC or international adjudication cores (refer the NZUDC adjudication policy).

Two supers (from different campuses) must approve your accreditation for you to be accredited. That means you must convince two super adjudicators that you will be a competitive member of the New Zealand adjudication pool. Often, accreditation decisions are made by consensus among the supers present at a tournament. In my experience, this is because vast differences in opinion as to whether someone meets the standard are rare; usually the supers at a tournament share similar views on trainees. However, consensus is not a requirement. In other words, no one super can “block” your accreditation if two others have been convinced you meet the standard.

# Who is involved in the accreditation process?

Often, there are more trainees than super adjudicators at a tournament. This means in some debates you may trainee under judges who are not super adjudicators. These judges do not have the power to accredit you. However, this does not mean their opinions do not matter. The adjudication core and supers at a tournament will make every effort to have you trainee under judges who are competitive and experienced. They will then listen carefully to the feedback these judges have on you and take it into account in making their decision.

# What happens in the judge room?

Discussions about trainees happen in the judge room. Every judge who has seen a trainee in that round will give feedback on their trainee covering such matters as:

- Whether they agreed with their trainee’s decisions (or, if there was disagreement, if they thought the split was reasonable).
- Whether the oral adjudication was concise, well-reasoned (as opposed to descriptive) and convincing; whether they had the trainee give the teams the oral adjudication.
- Whether the trainee’s scores were similar to theirs; whether the trainee was able to justify their scoring.
- Areas in which the trainee needs to improve, feedback that has been given to them, and what judges should look for when seeing the trainee in future rounds.
- Often, the judge will be asked to give the trainee a score based on the feedback scale being used for accredited judges at this tournament.

All judges usually participate in these discussions, but at the very least all supers and judges who have seen trainees in that round will participate.

A written record is usually kept of this discussion for later reference. At an NZUDC tournament, an adjudication core member will be formally “in charge” of trainees and will be tasked with keeping this record. At Thropy, Claytons or WGM, someone is likely to take on this role informally.

Based on these discussions, a decision to accredit someone can be made at any time during the tournament. Accreditations are, however, most commonly announced at break night or on the final day. The reason for this is that supers are looking for someone who is consistently at standard, not just someone who is good in one or two debates, and want a range of data points. This also means that if a trainee’s tournament does not start off well, or the trainee gives one judgment below standard, this does not mean they cannot come up to standard overall. A “bad” one or two debates is not necessarily the end of the world.

After trainee discussions, the adjudication core (and supers) will then allocate the trainees to rooms for the next round. Factors that might be taken into account in making allocation decisions are:

- Ensuring trainees see super adjudicators they have not seen previously.
- The likely quality of the room the trainee is being put on and how close the debate is likely to be.

# Why was I called back to trainee in the out-rounds?

In general, trainees will be called back for out-rounds when supers think the trainee may meet the standard but need more information to be certain. That is to say, the supers cannot accredit you now but think they might be able to if they give you another chance. Usually, a person being called back has delivered some good judgments, but has not been consistently at standard. A trainee will not be called back unless the supers think that person will demonstrate that they meet the accreditation standard if they perform well in the out-rounds.

For example, if the trainee split once, it may be possible to confirm that this was an anomaly by seeing them again in the out-rounds. Alternatively, a trainee might have shown improvement, but it is not clear yet if that improvement will be maintained. Another option might be that the trainee may happen to have not seen high quality and/or close debates, and the supers want to test them..

Calling trainees back is more common at a short form tournament, such as Thropy or Claytons, when there are very few in-rounds to collect information in.

Sometimes, it will unfortunately be the case that the information gathered about a trainee in the out-rounds is not favourable, and so the balance will weigh against their accreditation.

# What should I expect to happen in each debate?

In sum, you will need to:

- watch and judge the debate;
- come to a decision and prepare an oral adjudication and ballot; and
- present your oral adjudication to the judge(s).

You should be prepared to answer questions on a variety of topics including:

- on aspects of your reasoning (or points in the debate that you have not mentioned in your reasoning;
- justifying your scores and why different speakers were scored differently; and
- on individual feedback you would give to different speakers.

You may be asked to give an oral adjudication to the teams, possibly with minor amendments suggested by the judge(s) in the room. If you are not asked to give the oral, you should listen to the oral adjudication given and consider how it differs from the one you prepared.

# How long will I get to prepare my oral adjudication/ballot?

Your judge(s) may expect you to provide your result/ballot/oral quite quickly after the debate. There is no set time for how long you will get to prepare, however, the timing usually reflects how long the judge has taken to come to their own result/reasoning and the time it has taken to have any panel discussions. You can ask for a few minutes more time if you need to; you will not be penalised for doing so.

However, you should endeavour to make your decisions as promptly as possible for several reasons, including:

At international tournaments, there are strict time limits for how long judges can take to deliberate (e.g. at Worlds a judge/panel must deliberate and provide a result in 15 mins). New Zealand domestic tournaments do not apply strict time limits in the same way, however, there is an expectation that a judge will make their decision promptly and will not keep the teams/tournament waiting. As a trainee, you should practice complying with these standards. It may seem that you are being asked to do a lot of things in a short amount of time. However, that is what accredited judges are also being asked to do. The time you are given is symmetric with how long it is taking a competitive judge to prepare their oral adjudication, so you shouldn’t be taking too much longer.

As a practical note, trainee oral adjudications etc need to happen fairly quickly or the whole tournament gets delayed. A room with a trainee takes longer than a room with a judge; the tournament is waiting for these rooms.

# Why do the supers/judges care about my scoring?

The reason why scoring matters is because a speaker scores will affect whether a team breaks, and because speaker scores affect tab position/speaker prizes. If you score too high or too low, you advantage or disadvantage a team relative to everyone else. It is expected that a competitive judge will be able to score consistently with other judges in the pool, so this must be taken into account in the accreditation process. For context, the scoring of accredited judges at a tournament is also taken into account in deciding what rooms they will judge and whether they will break.

Scoring is difficult and takes practice. A (small) range of scores are reasonable in any given debate. It is particularly important that you justify your scores (both to yourself, and to your judge(s) if asked). Consider: Was the debate high or low quality overall? Which speakers were better/worse? Why?

If you receive feedback on your scoring, you should ask what scores the judge thinks have been appropriate.

Some judges will prepare a ballot from scratch at the end of the debate, but you may find it helpful to note down provisional scores as you go (e.g. first aff gave a 74/75 speech).

# Why am I being asked questions?

Your judge(s) may ask you questions for a variety of reasons, including:

- To give you the opportunity to expand on aspects of your reasoning they think need further development or are unclear.
- To challenge you on parts of your reasoning (this may be to see if you can appreciate a different perspective or to see if you could defend your reasoning if a team was to challenge you).
- To address content from the debate you have not mentioned in your oral (this may be because they think this content is important, or may be because they agree the content is unimportant but want you to explain why you think that).
- To have you to justify your scores.
- To have you articulate what individual feedback you would give speakers, especially good speakers (accredited judges are expected to provide good quality feedback as well as give an oral adjudication).

Take your time in replying to questions. Review your notes if you need to.

# Will I get feedback?

You should expect to receive feedback from the judge(s) in the room (except in a final, silent round, where a judge might not be able to give feedback without revealing the result); remind them if they forget! If you want further feedback or do not fully understand the feedback given you should ask questions. The judge(s) are here to help. You may also be able to receive a summary of your feedback from the adjudication core member in charge of adjudication at the end of a tournament.

# Can I give feedback?

If a judge acts in a way that you have concerns about and/or may be inequitable (e.g. making disparaging comments, refusing to answer questions, providing feedback that appears sexist), this is a matter you should take up with the adjudication core and/or the equity team. The adjudication core can, for example, place you with other judges for the remainder of the tournament and/or can discuss this person’s behaviour with them. As a trainee, you can conflict a judge if you have a genuine reason for doing so.

# Do I have give feedback to speakers?

You may be asked to give feedback to speakers if the teams want it, particularly if you had a different view of the debate compared to the judge.
