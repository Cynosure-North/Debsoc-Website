---
layout: sponsorship
title: Sponsorship
left-block: >
  ### The Need for Sponsorship


  Traditionally debating in New Zealand is self-funded, which places a large financial burden on speakers wishing to participate at the highest level. While teams from Australia, Asia, the United States and the United Kingdom all enjoy comprehensive funding, this remains a challenge for Victoria University of Wellington participants. Given the extremely high costs of attending international tournaments, corporate sponsorship is sought as a way to ensure our teams can continue to participate. 


  Funding could also be used to subsidise the domestic activities Vic Debsoc participates in, including the hosting of established New Zealand tournaments, such as Joynt Scroll, Officer's Cup and New Zealand Impromptu Debating championships.


  While establishing a partnership with a cornerstone sponsor would be desirable, any contribution to the team would be a substantial boost, and make a genuine difference to the team.


  Ultimately it is up to the sponsor to nominate the activities they wish to sponsor, and Victoria Debating Society is happy to work to find the use that best aligns with the sponsors strategic goals and values.
right-block: >
  ### Partnership Advantages


  Victoria University of Wellington Debating Society recognises that sponsorship needs to be a partnership, and is eager to provide any opportunities or benefits potential sponsors may wish to negotiate. Potential benefits include, but are not limited to:


  - Naming rights for a debating team or squad, to be used in any official media releases (for example, media releases would describe the team as “[Your Company Here] Vic debating”.

  - Recognition in any media releases relating to the area of your sponsorship (for example, “Victoria University of Wellington Debating Society would like to thank Your Company Here for its generous support for Project X”).

  - The opportunity to be the named host of a public debate. Vic Desoc would organise a public debate on a topic of your company’s choosing, and would endeavour to arrange debaters, venue and marketing.

  - The opportunity to utilise the Vic Debsoc as a recruitment partner. Vic Debsoc involves many of the university’s most accomplished young people, and we would be happy to arrange for the distribution of recruitment materials, or for a speaker to address one of our events.

  - The opportunity to use the services of Vic Debsoc at your next corporate event. Vic debaters have been used at several conferences to provide a unique combination of insightful commentary and entertainment. These debates have been very well received by conference goers. Similarly, Vic debaters are also able to run workshops based on the public speaking and analytical skills debating encourages. These fun events can also be used as a team building exercise, in addition to providing valuable skills.


  Vic Debosc hopes that this sponsorship opportunity could be used to create and enduring relationship between the society and the sponsor, and is open to negotiate for sponsorship on a long-term basis as well.
end-block: >
  ### Going Forward


  We hope you share our enthusiasm for the team and the opportunity sponsorship offers and we would love to meet you personally to discuss the possibility of sponsorship. If you have any questions at all, or would like to meet us to discuss this opportunity further, please do not hesitate to contact the society at the email address below.


  Thank you for your time,


  sponsorship@vicdebsoc.org.nz
---
